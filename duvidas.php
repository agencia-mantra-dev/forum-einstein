<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Dúvidas Frequentes | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/orfao.css" />
        <link rel="stylesheet" href="css/tooltipster.css" />
        <link rel="stylesheet" href="css/fancybox.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="ingles/index.php" class="eng">English</a></li>
                        <li><a href="espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> <span>como chegar</span></a></li>
                        <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> <span>hospedagem</span></a></li>
                        <li><a href="contato.php"><i class="fa fa-envelope"></i> <span>contato</span></a></li>
                        <li class="last select"><a href="duvidas.php"><i class="fa fa-question-circle"></i> <span>dúvidas frequentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="evento.php">evento</a></li>
                        <li><a href="programacao.php">programação</a></li>
                        <li><a href="palestrantes.php">palestrantes</a></li>
                        <li><a href="inscricoes.php">inscrições</a></li>
                        <li class="last"><a href="trabalhos.php">trabalhos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Dúvidas Frequentes</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central duvidas">
                <div class="col_left">
                    <dl>
                        <dt class="bt_click" rel="#duvida01">
                            <h2>Como faço para receber meu certificado?</h2>
                        </dt>
                        <dd id="duvida01" class="toggleDiv">O certificado de participação será enviado em formato eletrônico para o e-mail cadastrado no ato da inscrição no prazo de 10 dias úteis após o término do evento. A emissão do certificado dependerá do comparecimento do participante no evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida02">
                            <h2>Como faço para receber a nota fiscal correspondente ao pagamento da minha inscrição?</h2>
                        </dt>
                        <dd id="duvida02" class="toggleDiv">A nota fiscal será enviada em formato eletrônico para o e-mail cadastrado no ato da inscrição no prazo de 7 dias úteis após o término do evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida03">
                            <h2>Posso levar um acompanhante ao evento?</h2>
                        </dt>
                        <dd id="duvida03" class="toggleDiv">Todas as atividades do evento são restritas aos participantes devidamente inscritos e portando a credencial do evento. Não será permitido o acesso ou permanência de acompanhantes ou pessoas sem o crachá de identificação.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida04">
                            <h2>As apresentações dos palestrantes estarão disponíveis após o evento?</h2>
                        </dt>
                        <dd id="duvida04" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida05">
                            <h2>Onde poderei obter imagens do evento?</h2>
                        </dt>
                        <dd id="duvida05" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida06">
                            <h2>O evento oferece pontuação em programas de educação continuada?</h2>
                        </dt>
                        <dd id="duvida06" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida07">
                            <h2>Haverá serviço de van disponível a partir de algum ponto até o evento?</h2>
                        </dt>
                        <dd id="duvida07" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida08">
                            <h2>Há alguma tarifa diferenciada para os participantes do evento em hotéis da região?</h2>
                        </dt>
                        <dd id="duvida08" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida09">
                            <h2>Haverá wi-fi disponível aos participantes durante o evento?</h2>
                        </dt>
                        <dd id="duvida09" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida10">
                            <h2>Haverá serviço de guarda-volumes?</h2>
                        </dt>
                        <dd id="duvida10" class="toggleDiv">Sim. O serviço estará disponível gratuitamente aos participantes no local do evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida11">
                            <h2>É permitido filmar, fotografar ou efetuar registro de áudio durante o evento?</h2>
                        </dt>
                        <dd id="duvida11" class="toggleDiv">Não é permitido gravar, filmar ou fotografar sem prévia autorização expressa da comissão organizadora do evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida12">
                            <h2>Haverá serviço de atendimento médico disponível?</h2>
                        </dt>
                        <dd id="duvida12" class="toggleDiv">O evento contará com uma ambulância disponível para atendimentos emergenciais durante todo o período da programação.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida13">
                            <h2>É permitido fumar nas dependências do evento?</h2>
                        </dt>
                        <dd id="duvida13" class="toggleDiv">Não é permitido fumar nas dependências do evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida14">
                            <h2>É recomendado algum traje específico?</h2>
                        </dt>
                        <dd id="duvida14" class="toggleDiv">O evento não requer nenhum traje específico para participação. Recomendamos, no entanto, traje esporte fino ou social.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida15">
                            <h2>É possível trocar as opções de workshops previamente selecionadas no momento de minha inscrição?</h2>
                        </dt>
                        <dd id="duvida15" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida16">
                            <h2>Quais serviços de alimentação estão inclusos no valor da inscrição?</h2>
                        </dt>
                        <dd id="duvida16" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida17">
                            <h2>Há serviço de alimentação nos arredores?</h2>
                        </dt>
                        <dd id="duvida17" class="toggleDiv">Sim, anexo ao local do evento há duas praças de alimentação com mais de 25 opções. Os arredores do complexo também oferece opções variadas de restaurantes.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida18">
                            <h2>Quando poderei efetuar meu credenciamento?</h2>
                        </dt>
                        <dd id="duvida18" class="toggleDiv">Para os participantes do pré-curso, o credenciamento estará disponível a partir das 8h do dia 13 de Agosto. Para os participantes das demais atividades, o credenciamento poderá ser feito a partir das 8h do dia 14 de Agosto.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida19">
                            <h2>As atividades serão abertas para perguntas aos palestrantes?</h2>
                        </dt>
                        <dd id="duvida19" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida20">
                            <h2>Em quais momentos poderei me relacionar com outros participantes?</h2>
                        </dt>
                        <dd id="duvida20" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida21">
                            <h2>Órgãos de imprensa terão acesso ao evento? Como proceder para participar?</h2>
                        </dt>
                        <dd id="duvida21" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida22">
                            <h2>Como solicitar uma necessidade especial?</h2>
                        </dt>
                        <dd id="duvida22" class="toggleDiv">Portadores de necessidades especiais deverão encaminhar um e-mail através da página de "Contato" com a descrição da solicitação desejada.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida23">
                            <h2>Há estacionamento disponível no local do evento?</h2>
                        </dt>
                        <dd id="duvida23" class="toggleDiv">Sim, o Complexo World Trade Center conta com 1.700 vagas cobertas além de serviço de Vallet. O CENU, complexo anexo ao WTC, possui mais 3.700 vagas com acesso coberto ao WTC Events Center. Esse serviço, no entanto, não está incluído no valor da inscrição e é de responsabilidade de cada participante.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida24">
                            <h2>Haverá serviço de tradução simultânea? Para quais idiomas?</h2>
                        </dt>
                        <dd id="duvida24" class="toggleDiv">Sim. Todas as atividades do evento serão traduzidas, permitindo com que sejam acompanhadas em Português, Inglês e Espanhol. Os receptores poderão ser retirados no local do evento mediante a entrega de um documento de identificação com foto. O documento será devolvido ao participante no momento da devolução do equipamento. </dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida25">
                            <h2>Caso haja perda de algum item, haverá algum ponto de achados e perdidos?</h2>
                        </dt>
                        <dd id="duvida25" class="toggleDiv">Sim, o serviço estará disponível na secretaria do evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida26">
                            <h2>Não consegui me inscrever com antecedência. Posso efetuar minha inscrição no local?</h2>
                        </dt>
                        <dd id="duvida26" class="toggleDiv">Serão disponibilizadas inscrições no local do evento mediante a disponibilidade de vagas de cada atividade. Consulte a política de preço para inscrições nesse período no menu "Inscrições".</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida27">
                            <h2>Perdi o meu crachá. Como posso ter acesso ao evento no dia seguinte?</h2>
                        </dt>
                        <dd id="duvida27" class="toggleDiv">Os participantes deverão se dirigir ao balcão de credenciamento e solicitar a 2ª via do crachá de identificação. Entretanto, esquecimento ou perda da credencial poderá acarretar em um custo adicional para ser reemitido.</dd>
                    </dl>
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip"  title="Marque este evento no seu calendário"></i> <i class="fa fa-calendar"></i></a> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais.</p>
                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery.tooltipster.min.js"></script>
        <script src="js/plugin/jquery.fancybox.js"></script>
        <script src="js/all.js"></script>
        <script src="js/duvidas.js"></script>

    </body>
</html>