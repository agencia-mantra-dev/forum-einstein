<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Inscripciones | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe en el mayor foro de América Latina sobre calidad y seguridad en salud, donde habrá presentaciones de prestigiosos conferencistas brasileños y de otros países" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/internas.css" />
        <link rel="stylesheet" href="../css/orfao.css" />
        <link rel="stylesheet" href="../css/tooltipster.css" />
        <link rel="stylesheet" href="../css/fancybox.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body id="espanhol">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="../index.php" class="port">Português</a></li>
                        <li><a href="../ingles/index.php" class="eng">English</a></li>
                    </ul>
                    <div class="social">
                        <p>Compártelo:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/espanhol/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe en el mayor foro de América Latina sobre calidad y seguridad en salud - http://goo.gl/yjEGAn&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="../images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="sede.php"><i class="fa fa-map-marker"></i> <span>Sede</span></a></li>
                        <li><a href="alojamiento.php"><i class="fa fa-suitcase"></i> <span>Alojamiento</span></a></li>
                        <li><a href="contacto.php"><i class="fa fa-envelope"></i> <span>Contacto</span></a></li>
                        <li class="last"><a href="preguntas.php"><i class="fa fa-question-circle"></i> <span>Preguntas Más Frecuentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="presentacion.php">Presentación</a></li>
                        <li><a href="programa.php">Programa</a></li>
                        <li><a href="conferencistas.php">Conferencistas</a></li>
                        <li><a href="inscripciones.php" class="select">Inscripciones</a></li>
                        <li class="last"><a href="trabajos.php">trabajos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Inscripciones</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central inscricoes">
                <div class="col_left">

                    <div id="tabs">
                        <ul class="abas">
                            <li class="item1 selected"><a href="#item1"><span>Inversión</span></a></li>
                            <li class="item2"><a href="#item2"><span>Políticas de<br>Descuentos</span></a></li>
                            <li class="item3"><a href="#item3"><span>Cancelamiento</span></a></li>
                            <li class="item4"><a href="#item4"><span>Información</span></a></li>
                        </ul>
                        <div id="item1" class="content_aba">
                            <h2>INVERSIÓN</h2>
                            <div class="table">
                                <span class="tit_table border">TABLA DE VALORES – PRECIOS POR PERSONA</span>

                                <!-- Dia 1 -->
                                <div class="esq">
                                    <span class="categ border">CATEGORÍA</span>
                                    <span class="linha_simple border">Congresista</span>
                                    <span class="linha_simple border">Estudiante<br>Empleado del Gobierno</span>
                                    <span class="linha_simple border">Miembro de órganos apoyadores</span>
                                    <span class="linha_simple border">Visita después de la conferencia (16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border">1ª FASE: hasta el 05/04/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border">Pre-curso + Conferencia<br>(3 días: 13 hasta 15/08)</span>
                                        <span class="preco border">$ 700</span>
                                        <span class="preco border duplo">$ 525</span>
                                        <span class="preco border duplo">$ 613</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border">Conferencia<br>(2 días: 14 hasta 15/08)</span>
                                        <span class="preco border">$ 560</span>
                                        <span class="preco border duplo">$ 420</span>
                                        <span class="preco border duplo">$ 490</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border">Día solo<br>(1 día: 13, 14 o 15/08)</span>
                                        <span class="preco border">$ 420</span>
                                        <span class="preco border duplo">$ 315</span>
                                        <span class="preco border duplo">$ 368</span>
                                    </span>
                                    <span class="fase border duplo none">$ 200</span>
                                </div>
                                <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                                <hr><br><br><br><br><br><br>
                                <!-- Dia 2 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORÍA</span>
                                    <span class="linha_simple border cinza">Congresista</span>
                                    <span class="linha_simple border cinza">Estudiante<br>Empleado del Gobierno</span>
                                    <span class="linha_simple border cinza">Miembro de órganos apoyadores</span>
                                    <span class="linha_simple border cinza">Visita después de la conferencia (16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">2ª FASE: desde 06/04/2015 hasta 07/06/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pre-curso + Conferencia<br>(3 días: 13 hasta 15/08)</span>
                                        <span class="preco border">$ 850</span>
                                        <span class="preco border duplo">$ 638</span>
                                        <span class="preco border duplo">$ 744</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferencia<br>(2 días: 14 hasta 15/08)</span>
                                        <span class="preco border">$ 680</span>
                                        <span class="preco border duplo">$ 510</span>
                                        <span class="preco border duplo">$ 595</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Día solo<br>(1 día: 13, 14 o 15/08)</span>
                                        <span class="preco border">$ 510</span>
                                        <span class="preco border duplo">$ 383</span>
                                        <span class="preco border duplo">$ 446</span>
                                    </span>
                                    <span class="fase border duplo none">$ 200</span>
                                </div>
                                <hr>
                                <!-- Dia 3 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORÍA</span>
                                    <span class="linha_simple border cinza">Congresista</span>
                                    <span class="linha_simple border cinza">Estudiante<br>Empleado del Gobierno</span>
                                    <span class="linha_simple border cinza">Miembro de órganos apoyadores</span>
                                    <span class="linha_simple border cinza">Visita después de la conferencia (16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">3ª FASE: desde 08/06/2015 hasta 06/08/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pre-curso + Conferencia<br>(3 días: 13 hasta 15/08)</span>
                                        <span class="preco border">$ 950</span>
                                        <span class="preco border duplo">$ 713</span>
                                        <span class="preco border duplo">$ 831</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferencia<br>(2 días: 14 hasta 15/08)</span>
                                        <span class="preco border">$ 760</span>
                                        <span class="preco border duplo">$ 570</span>
                                        <span class="preco border duplo">$ 665</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Día solo<br>(1 día: 13, 14 o 15/08)</span>
                                        <span class="preco border">$ 570</span>
                                        <span class="preco border duplo">$ 428</span>
                                        <span class="preco border duplo">$ 499</span>
                                    </span>
                                    <span class="fase border duplo none">$ 200</span>
                                </div>
                                <hr>
                                <!-- Dia 4 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORÍA</span>
                                    <span class="linha_simple border cinza">Congresista</span>
                                    <span class="linha_simple border cinza">Estudiante<br>Empleado del Gobierno</span>
                                    <span class="linha_simple border cinza">Miembro de órganos apoyadores</span>
                                    <span class="linha_simple border cinza">Visita después de la conferencia (16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">4ª FASE (LOCAL DEL EVENTO): desde 13/08/2015 hasta 16/08/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pre-curso + Conferencia<br>(3 días: 13 hasta 15/08)</span>
                                        <span class="preco border">$ 1000</span>
                                        <span class="preco border duplo">$ 750</span>
                                        <span class="preco border duplo">$ 875</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferencia<br>(2 días: 14 hasta 15/08)</span>
                                        <span class="preco border">$ 800</span>
                                        <span class="preco border duplo">$ 600</span>
                                        <span class="preco border duplo">$ 700</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Día solo<br>(1 día: 13, 14 o 15/08)</span>
                                        <span class="preco border">$ 600</span>
                                        <span class="preco border duplo">$ 450</span>
                                        <span class="preco border duplo">$ 525</span>
                                    </span>
                                    <span class="fase border duplo none">$ 200</span>
                                </div>

                            </div>
                        </div>
                        <div id="item2" class="content_aba" style="display:none">
                            <h2>POLÍTICAS DE DESCUENTOS Y TARIFAS ESPECIALES</h2>
                            <p>El Foro Anual Latinoamericano de Calidad y Seguridad en Salud ofrecerá una cantidad limitada de plazas con valores diferenciados para grupos y para categorías específicas de participantes. Esos descuentos se aplican al pre-curso y a la conferencia y NO se aplican a las visitas monitoreadas post-conferencia y demás actividades opcionales.</p>

                            <div class="tarifas">
                                <div class="tit border">Tarifas especiales</div>
                                <div class="txt border">
                                    <p>Al optar por esta categoría, cerciórese de que haya registrada de forma correcta en su inscripción la institución de enseñanza (para estudiantes) o la institución en que actúa (para miembros de instituciones de apoyo/ empleado del Gobierno). A la hora de inscribirse, Usted tendrá que presentar alguna evidencia de su vínculo con la institución o de enseñanza o de trabajo. El no tener alguna prueba implica tener que pagar la diferencia entre el valor que pagó y el valor integral de la inscripción.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Estudiantes y residentes</div>
                                <div class="txt border">
                                    <p>Válido para estudiantes de cursos de grado, post-grado o residencia debidamente matriculados en la fecha del evento.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Empleados del Hospital Israelita Albert Einstein</div>
                                <div class="txt border">
                                    <p>Válido para empleados registrados y miembros del Cuerpo Clínico abierto. </p>
                                    <p>Los empleados del Einstein portadores de DRT podrán, adicionalmente, enviar solicitud para apoyo financiero.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Empleados de órganos de gobierno</div>
                                <div class="txt border">
                                    <p>Válido para empleados de instituciones públicas y órganos de gobierno.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Miembros de órganos<br>que apoyan el evento</div>
                                <div class="txt border">
                                    <p>Válido para miembros de las sociedades, instituciones y órganos que apoyan el evento debidamente asociados en la fecha del evento. </p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Descuento para grupos</div>
                                <div class="txt border">
                                    <p>Grupos de tres o más personas de una misma organización son elegibles para recibir descuentos sobre la tarifa de participantes individuales.</p>
                                    <p>
                                        <strong>De 3 a 10 personas</strong>: 10% de descuento<br>
                                        <strong>Más de 10 personas</strong>: 20% de descuento<br>
                                        <strong>Inscripciones de grupo</strong>: Dudas y solicitudes se deben enviar para el e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a>.
                                    </p>
                                </div>
                            </div>
                            <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div id="item3" class="content_aba" style="display:none">
                            <h2>REGLAS DE CANCELAMIENTO</h2>
                            <p>Solamente se aceptarán pedidos de cancelamientos formalizados a través de e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a> hasta el día 24 de julio de 2015. El reembolso será deL 80% del valor invertido y será realizado en el plazo de 30 días a partir de la fecha de entrega de la documentación necesaria. Después de esa fecha no habrá resarcimiento.</p>

                            <h2>SUSTITUCIONES</h2>
                            <p>Solamente se aceptarán pedidos de sustituciones formalizados a través del e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a> hasta el día 06 de Agosto de 2015</p>
                            <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div id="item4" class="content_aba" style="display:none">
                            <h2>CERTIFICADO</h2>
                            <p>El certificado de participación será enviado en formato electrónico al e-mail registrado en la inscripción en un plazo de 10 días hábiles después de terminar el evento.</p>

                            <h2>CARGA HORARIA</h2>
                            <p>
                                Pre-curso: 7 horas<br>
                                Conferencia: 13 horas<br>
                                Visita monitoreada post-conferencia: 3 horas
                            </p>

                            <h2>PLAZAS</h2>
                            <p>
                                Pre-curso: 1.000<br>
                                Conferencia: 2.000<br>
                                Visita post-conferencia: 50 por módulo
                            </p>

                            <h2>FORMAS DE PAGO</h2>
                            <p>En breve</p>

                            <h2>IDENTIFICACIÓN</h2>
                            <p>La credencial de acceso al evento es personal e intransferible. Su uso es obligatorio en todas las actividades, tanto para acceso como para permanencia en las dependencias del evento. El olvido o la pérdida de la credencial podrá acarrear en un costo adicional para emitir una 2ª copia.</p>

                            <h2>COMPROBANTE DE PAGO</h2>
                            <p>La factura referente al pago de la tasa de inscripción será enviado al e-mail registrado en el momento de la inscripción en el plazo de 7 días hábiles después de terminar el evento. </p>
                            <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="../vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip" title="Marque este acontecimiento en su calendario"></i> <i class="fa fa-calendar"></i></a> <p><strong>Fecha</strong><br>13-16 de Agosto del 2015</p>
                    </div>
                    <p class="txt">Participe en el mayor foro de América Latina sobre calidad y seguridad en salud, donde habrá presentaciones de prestigiosos conferencistas brasileños y de otros países.</p>
                    <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>newsletter</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nombre" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Título Profesional" class="border">
                                <input type="text" placeholder="Negocios" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compártelo:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/espanhol/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe en el mayor foro de América Latina sobre calidad y seguridad en salud - http://goo.gl/yjEGAn&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> sede</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nombre" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Título Profesional" class="border">
                            <input type="text" placeholder="Negocios" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> Contacto</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nombre" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensaje"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="../images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Reservados todos los derechos</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="../images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery.tooltipster.min.js"></script>
        <script src="../js/plugin/jquery.fancybox.js"></script>
        <script src="../js/all.js"></script>
        <script src="../js/inscricoes.js"></script>

    </body>
</html>