<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Preguntas Más Frecuentes | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe en el mayor foro de América Latina sobre calidad y seguridad en salud, donde habrá presentaciones de prestigiosos conferencistas brasileños y de otros países" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/internas.css" />
        <link rel="stylesheet" href="../css/orfao.css" />
        <link rel="stylesheet" href="../css/tooltipster.css" />
        <link rel="stylesheet" href="../css/fancybox.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body id="espanhol">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="../index.php" class="port">Português</a></li>
                        <li><a href="../ingles/index.php" class="eng">English</a></li>
                    </ul>
                    <div class="social">
                        <p>Compártelo:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/espanhol/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe en el mayor foro de América Latina sobre calidad y seguridad en salud - http://goo.gl/yjEGAn&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="../images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="sede.php"><i class="fa fa-map-marker"></i> <span>Sede</span></a></li>
                        <li><a href="alojamiento.php"><i class="fa fa-suitcase"></i> <span>Alojamiento</span></a></li>
                        <li><a href="contacto.php"><i class="fa fa-envelope"></i> <span>Contacto</span></a></li>
                        <li class="last select"><a href="preguntas.php"><i class="fa fa-question-circle"></i> <span>Preguntas Más Frecuentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="presentacion.php">Presentación</a></li>
                        <li><a href="programa.php">Programa</a></li>
                        <li><a href="conferencistas.php">Conferencistas</a></li>
                        <li><a href="inscripciones.php">Inscripciones</a></li>
                        <li class="last"><a href="trabajos.php">trabajos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Preguntas Más Frecuentes</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central duvidas">
                <div class="col_left">
                    <dl>
                        <dt class="bt_click" rel="#duvida01">
                            <h2>¿Cómo hago para recibir mi certificado?</h2>
                        </dt>
                        <dd id="duvida01" class="toggleDiv">El certificado de participación será enviado en formato electrónico al e-mail de la persona registrada durante la inscripción, en un plazo de 10 días hábiles después del término del evento. La emisión del certificado depende de la presencia del participante en el evento.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida02">
                            <h2>¿Cómo hago para recibir la factura correspondiente al pago de mi inscripción?</h2>
                        </dt>
                        <dd id="duvida02" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida03">
                            <h2>¿Puedo llevar un acompañante al evento?</h2>
                        </dt>
                        <dd id="duvida03" class="toggleDiv">Todas las actividades del evento son restringidas a los participantes debidamente inscritos y portando la credencial del evento. No será permitido el acceso o permanencia de acompañantes o personas sin la credencial de identificación.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida04">
                            <h2>¿Las presentaciones de los conferencistas estarán disponibles después del evento?</h2>
                        </dt>
                        <dd id="duvida04" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida05">
                            <h2>¿Dónde podré obtener imágenes del evento?</h2>
                        </dt>
                        <dd id="duvida05" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida06">
                            <h2>¿El evento ofrece puntuación en programas de educación continuada?</h2>
                        </dt>
                        <dd id="duvida06" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida07">
                            <h2>¿Habrá servicio de microómnibus disponible a partir de algún punto hasta el evento?</h2>
                        </dt>
                        <dd id="duvida07" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida08">
                            <h2>¿Hay alguna tarifa especial para los participantes del evento en hoteles de la región?</h2>
                        </dt>
                        <dd id="duvida08" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida09">
                            <h2>¿Habrá wi-fi disponible para los participantes durante el evento?</h2>
                        </dt>
                        <dd id="duvida09" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida10">
                            <h2>¿Habrá servicio de guardería?</h2>
                        </dt>
                        <dd id="duvida10" class="toggleDiv">Si. El servicio estará a disposición. de forma gratuita, para los  participantes en el local del evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida11">
                            <h2>¿Se permite filmar, fotografiar o grabar audio durante el evento?</h2>
                        </dt>
                        <dd id="duvida11" class="toggleDiv">No se permite grabar, filmar o fotografiar sin la autorización previa expresa de la comisión de organización del evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida12">
                            <h2>¿Habrá servicio de primeros auxilios?</h2>
                        </dt>
                        <dd id="duvida12" class="toggleDiv">El evento contará con una ambulancia a disposición para atender cualquier emergencia durante todo el periodo de la programación.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida13">
                            <h2>¿Se permite fumar en las dependencias del evento?</h2>
                        </dt>
                        <dd id="duvida13" class="toggleDiv">No está permitido fumar en las instalaciones del evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida14">
                            <h2>¿Se recomienda algún vestuario específico?</h2>
                        </dt>
                        <dd id="duvida14" class="toggleDiv">El evento no requiere  el uso de ningún traje específico para La participación. Sin embargo recomendamos traje fino deportivo o social.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida15">
                            <h2>¿Es posible cambiar las opciones de workshops previamente seleccionadas en el momento de mi inscripción?</h2>
                        </dt>
                        <dd id="duvida15" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida16">
                            <h2>¿Qué servicios de alimentación están incluidos en el valor de la inscripción?</h2>
                        </dt>
                        <dd id="duvida16" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida17">
                            <h2>¿Hay servicios de alimentación en los alrededores?</h2>
                        </dt>
                        <dd id="duvida17" class="toggleDiv">Si, adyacente al local del evento hay dos plazas de alimentación, con más de 25 opciones. En los alrededores del complejo hay opciones variadas de restaurantes.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida18">
                            <h2>¿Cuándo podré hacer mi acreditación?</h2>
                        </dt>
                        <dd id="duvida18" class="toggleDiv">Para los participantes del pre-curso, La acreditación estará disponible a partir de las 8h del día 13 de Agosto. Para  participantes de las demás actividades, La acreditación podrá hacerse a partir de las 8h del día 14 de Agosto. </dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida19">
                            <h2>¿Las actividades serán abiertas para preguntas a los conferencistas?</h2>
                        </dt>
                        <dd id="duvida19" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida20">
                            <h2>¿En qué momento podré relacionarme con otros participantes?</h2>
                        </dt>
                        <dd id="duvida20" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida21">
                            <h2>¿Órganos de prensa tendrán acceso al evento? Cómo proceder para participar?</h2>
                        </dt>
                        <dd id="duvida21" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida22">
                            <h2>¿Cómo solicitar una necesidad especial?</h2>
                        </dt>
                        <dd id="duvida22" class="toggleDiv">Portadores de necesidades especiales deben enviar un e-mail por medio de la página de "Contacto" con la descripción y el pedido deseado.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida23">
                            <h2>¿Hay estacionamiento disponible en el lugar del evento?</h2>
                        </dt>
                        <dd id="duvida23" class="toggleDiv">Si, el Complejo World Trade Center cuenta con 1.700 lugares cubiertos, además del servicio Valet. El CENU, complejo adyacente al  WTC, tiene un adicional de 3.700 lugares con acceso cubierto al WTC Events Center. Sin embargo, ese servicio no está incluido en el valor de la inscripción y será de plena responsabilidad de cada participante.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida24">
                            <h2>¿Habrá servicio de traducción simultánea? ¿En qué idiomas?</h2>
                        </dt>
                        <dd id="duvida24" class="toggleDiv">Sí. Todas las actividades del evento serán traducidas, permitiendo que puedan acompañarlas en Portugués, Inglés y Español. Los audífonos podrán retirarlos en el lugar del evento mediante la entrega de un documento de identificación con foto. El documento se le devolverá al participante al devolver el equipo.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida25">
                            <h2>¿En caso de pérdida de algún objeto, habrá algún punto de objetos perdidos?</h2>
                        </dt>
                        <dd id="duvida25" class="toggleDiv">Si, el servicio estará a disposición en la secretaría del evento.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida26">
                            <h2>No pude inscribirme con anticipación. ¿Puedo hacerlo al llegar al lugar del evento?</h2>
                        </dt>
                        <dd id="duvida26" class="toggleDiv">Se pondrá a disposición lugares de inscripción, dependiendo del número de vacancias de cada actividad. Consulte la política de precio para las inscripciones durante ese período en el menú "Inscripciones".</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida27">
                            <h2>Perdí mi credencial. ¿Cómo puedo tener acceso al evento al día siguiente?</h2>
                        </dt>
                        <dd id="duvida27" class="toggleDiv">Los participantes deberán dirigirse al mostrador de acreditación para solicitar la copia de sus credenciales. No obstante, el hecho de haber olvidado o perdido la credencial puede significar un costo adicional para su envío.</dd>
                    </dl>
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="../vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip" title="Marque este acontecimiento en su calendario"></i> <i class="fa fa-calendar"></i></a> <p><strong>Fecha</strong><br>13-16 de Agosto del 2015</p>
                    </div>
                    <p class="txt">Participe en el mayor foro de América Latina sobre calidad y seguridad en salud, donde habrá presentaciones de prestigiosos conferencistas brasileños y de otros países.</p>
                    <a href="inscricao.php" class="inscrevase border">Inscríbase <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>newsletter</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nombre" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Título Profesional" class="border">
                                <input type="text" placeholder="Negocios" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compártelo:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/espanhol/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe en el mayor foro de América Latina sobre calidad y seguridad en salud - http://goo.gl/yjEGAn&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> sede</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nombre" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Título Profesional" class="border">
                            <input type="text" placeholder="Negocios" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> Contacto</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nombre" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensaje"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="../images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Reservados todos los derechos</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="../images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery.tooltipster.min.js"></script>
        <script src="../js/plugin/jquery.fancybox.js"></script>
        <script src="../js/all.js"></script>
        <script src="../js/duvidas.js"></script>

    </body>
</html>