<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Evento | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/evento.css" />
        <link rel="stylesheet" href="css/tooltipster.css" />
        <link rel="stylesheet" href="css/fancybox.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="ingles/index.php" class="eng">English</a></li>
                        <li><a href="espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> <span>como chegar</span></a></li>
                        <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> <span>hospedagem</span></a></li>
                        <li><a href="contato.php"><i class="fa fa-envelope"></i> <span>contato</span></a></li>
                        <li class="last"><a href="duvidas.php"><i class="fa fa-question-circle"></i> <span>dúvidas frequentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="evento.php" class="select">evento</a></li>
                        <li><a href="programacao.php">programação</a></li>
                        <li><a href="palestrantes.php">palestrantes</a></li>
                        <li><a href="inscricoes.php">inscrições</a></li>
                        <li class="last"><a href="trabalhos.php">trabalhos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Evento</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central">
                <div class="col_left">
                    <!--a href="modal/submissao.php" class="submissao border iframe">submissão de trabalhos <i class="fa fa-file-text"></i></a-->
                    
                    <h2>sobre o evento</h2>
                    <p>Qualidade e segurança no atendimento à saúde são questões fundamentais que precisam ser endereçadas para dar suporte à sustentabilidade nesse setor. Ao considerarmos a sustentabilidade, devemos incluir também os fatores financeiros, ambientais, sociais e éticos. “Em Busca da Sustentabilidade” é o tema do <strong>1º Fórum Latino Americano de Qualidade e Segurança na Saúde</strong> a ser realizado de 13 a 16 de agosto de 2015 em São Paulo, Brasil.</p>
                    <p>Nestes quatro dias de congresso, médicos, profissionais da qualidade e segurança, líderes e muitos outros se reunirão para compartilhar suas experiências e para debaterem profundamente questões importantes na saúde contemporânea, tais como a segurança do paciente, custo/benefício na saúde, eficiência, melhoria da qualidade, sustentabilidade ambiental, inovação, gestão populacional, responsabilidade do indivíduo pela própria saúde, conhecimento sobre saúde populacional e para pacientes e muitos outros tópicos importantes.</p>
                    <p>O Fórum Latino Americano oferecerá um diálogo saudável entre o melhor que existe na teoria, com apresentações de palestrantes renomados, e o melhor da prática, com casos de instituições locais de saúde que servem de benchmarks para a região.</p>
                    <p class="bold">Venha participar do maior congresso da América Latina sobre qualidade e segurança na saúde.</p>
                    
                    <h2>mais informações</h2>
                    <p>O <strong>Hospital Israelita Albert Einstein</strong>, uma instituição de saúde brasileira com mais de 50 anos de história na busca por uma melhor qualidade e maior segurança na saúde, orgulha-se em se associar ao <a href="http://www.ihi.org/" target="_blank">Institute for Healthcare Improvement (IHI)</a> - uma organização sem fins lucrativos baseada nos EUA e com mais de 25 anos de experiência em inovação, organização, parceria e fomento de resultados na saúde e na melhoria do atendimento à saúde em todo o mundo - para trazer a todos o 1º Fórum Latino Americano de Qualidade e Segurança na Saúde.</p>
                    <p>O Fórum Latino Americano baseia-se no histórico bem fundamentado e sucedido do anual National Forum on Quality Improvement in Health Care, na América do Norte, do IHI, que agora se encontra em sua 26ª edição, e no Fórum Internacional anual na Europa, agora em sua 20ª edição.</p>
                    <p>Estes Fóruns são as principais conferências para as pessoas que compartilham a missão de melhorar o atendimento à saúde em termos de qualidade e segurança, reunindo milhares de participantes de todas as partes do mundo a cada ano. Baseado nos Fóruns do IHI, o Fórum Latino Americano de Qualidade e Segurança na Saúde focará nas questões de maior importância para a melhoria da qualidade e segurança no atendimento à saúde no Brasil e na América Latina.</p>
                    
                    <h2>público alvo</h2>
                    <p>Este evento é indicado a todas as pessoas interessadas na melhoria da qualidade e da segurança do paciente, bem como àqueles que buscam a sustentabilidade no atendimento à saúde.</p>
                    <ul class="simple_list">
                        <li>- Lideranças</li>
                        <li>- Profissionais de qualidade</li>
                        <li>- Profissionais de segurança</li>
                        <li>- Profissionais de enfermagem</li>
                        <li>- Médicos</li>
                        <li>- Residentes</li>
                        <li>- Estudantes da área da saúde</li>
                        <li>- Administradores</li>
                        <li>- Pesquisadores</li>
                        <li>- Funcionários da linha de frente no atendimento ao paciente</li>
                    </ul>
                    
                    <h2>pontuação</h2>
                    <p>Em breve</p>
                    <!--div class="tabela">
                        <span class="tb_tr border">pontuação</span>
                        <span class="row">
                            <span class="blue celula border double">X pontos</span>
                            <span class="celula border">Programa de Educação Médica Continuada<br>do Hospital Israelita Albert Einstein</span>
                        </span>
                        <span class="row">
                            <span class="blue celula border mob2lin">Y pontos</span>
                            <span class="celula border">Comissão Nacional de Acreditação</span>
                        </span>
                        <span class="row">
                            <span class="blue celula border double">Z pontos</span>
                            <span class="celula border">Programa de Educação Médica Continuada<br>do Hospital Israelita Albert Einstein</span>
                        </span>
                    </div-->

                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip"  title="Marque este evento no seu calendário"></i> <i class="fa fa-calendar"></i></a> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais.</p>
                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery.tooltipster.min.js"></script>
        <script src="js/plugin/jquery.fancybox.js"></script>
        <script src="js/all.js"></script>
        <script src="js/evento.js"></script>

    </body>
</html>