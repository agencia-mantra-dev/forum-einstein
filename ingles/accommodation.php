<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Accommodation and Tours | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="We invite you to join the largest conference in Latin America on health quality and safety together with the presentation of world known national and international speakers" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/internas.css" />
        <link rel="stylesheet" href="../css/orfao.css" />
        <link rel="stylesheet" href="../css/tooltipster.css" />
        <link rel="stylesheet" href="../css/fancybox.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body id="ingles">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="../index.php" class="port">Português</a></li>
                        <li><a href="../espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="../images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="directions.php"><i class="fa fa-map-marker"></i> <span>Venue & Directions</span></a></li>
                        <li class="select"><a href="accommodation.php"><i class="fa fa-suitcase"></i> <span>Accommodation</span></a></li>
                        <li><a href="contact.php"><i class="fa fa-envelope"></i> <span>Contact</span></a></li>
                        <li class="last"><a href="faq.php"><i class="fa fa-question-circle"></i> <span>faq</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">about</a></li>
                        <li><a href="program.php">Program</a></li>
                        <li><a href="speakers.php">Speakers</a></li>
                        <li><a href="registration.php">Registration</a></li>
                        <li class="last"><a href="posters.php">posters</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Accommodation and Tours</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central hospedagem">
                <div class="col_left">
                    <p>Check here information about accommodations, transportation, and well-being for a pleasant stay in São Paulo</p>

                    <h2>AD Ag. de Viagens e Turismo</h2>
                    <p>
                        <span class="bold">Contact person</span>: Carla Klajner<br>
                        <span class="bold">E-mail</span>: ihi@adturismo.com.br <br>
                        <span class="bold">Addres</span>: Rua Estela, 164 - São Paulo, SP<br>
                        <span class="bold">Tel.</span>: +55 11 5087-3455<br>
                        <span class="bold">Fax.</span>: +55 11 5087-3455
                    </p>

                    <h2>HOTELS</h2>
                    <p>Below a list of hotels located close to the venue. For more information on room availability and differentiated rates for participants, please check with the official tour agency.</p>
                    <p>
                        <span class="bold true">Sheraton São Paulo WTC</span><br>
                        <span class="bold">Address</span>: Av. Das Nações Unidas 12.559 - Brooklin Novo<br>
                        <span class="bold">Location</span>: on the same building of the venue<br>
                        <a href="http://www.sheratonsaopaulowtc.com" target="_blank">www.sheratonsaopaulowtc.com</a>
                    </p>
                    <p>
                        <span class="bold true">Gran Estanplaza São Paulo</span><br>
                        <span class="bold">Address</span>: Rua Arizona, 1.517 - Brooklin Novo<br>
                        <span class="bold">Location</span>: right across São Paulo WTC Sheraton<br>
                        <a href="http://www.estanplaza.com.br/gran-estanplaza/institucional" target="_blank">www.estanplaza.com.br/gran-estanplaza/institucional</a>
                    </p>
                    <p>
                        <span class="bold true">Grand Hyatt São Paulo</span><br>
                        <span class="bold">Address</span>: Av. Das Nações Unidas, 13.301 – Brooklin Novo<br>
                        <span class="bold">Location</span>: approximately 1 km from venue<br>
                        <a href="http://saopaulo.grand.hyatt.com" target="_blank">http://saopaulo.grand.hyatt.com</a>
                    </p>
                    <p>
                        <span class="bold true">Hilton São Paulo Morumbi</span><br>
                        <span class="bold">Address</span>: Av. Das Nações Unidas, 12.901 – Brooklin Novo<br>
                        <span class="bold">Location</span>: on the same complex of buildings where WTC is located<br>
                        <a href="http://www3.hilton.com" target="_blank">www3.hilton.com</a>
                    </p>
                    <p>  
                        <span class="bold true">Transamérica Executive</span><br>
                        <span class="bold">Address</span>: Rua Américo Brasiliense, 2.163<br>
                        <span class="bold">Bairro</span>: Chácara Santo Antônio<br>
                        <span class="bold">Location</span>: 3 km from venue<br>
                        <a href="http://www.transamericagroup.com.br/br/nossos-hoteis/executive/chacara" target="_blank">www.transamericagroup.com.br/br/nossos-hoteis/executive/chacara</a>
                    </p>
                    <p>
                        <span class="bold true">Transamérica Executive Congonhas</span><br>
                        <span class="bold">Address</span>: Rua Vieira de Morais, 1960 – Campo Belo<br>
                        <span class="bold">Location</span>: 7 km from venue<br>
                        <a href="http://www.transamericagroup.com.br/br/nossos-hoteis/executive/congonhas" target="_blank">www.transamericagroup.com.br/br/nossos-hoteis/executive/congonhas</a>
                    </p>
                    <p>
                        <span class="bold true">Blue Tree Morumbi </span><br>
                        <span class="bold">Address</span>: Av. Roque Petroni Junior, 1000 – Brooklin Novo<br>
                        <span class="bold">Location</span>: 2,5 km from venue<br>
                        <a href="http://www.bluetree.com.br/hotel/blue-tree-premium-morumbi" target="_blank">www.bluetree.com.br/hotel/blue-tree-premium-morumbi</a>
                    </p>
                    <p>
                        <span class="bold true">Blue Tree Premium Verbo Divino</span><br>
                        <span class="bold">Address</span>: Rua Verbo Divino, 1.323 – Chácara Santo Antonio<br>
                        <span class="bold">Location</span>: 3,5 km from venue<br>
                        <a href="http://www.bluetree.com.br/hotel/blue-tree-premium-verbo-divino" target="_blank">www.bluetree.com.br/hotel/blue-tree-premium-verbo-divino</a>
                    </p>
                    <p>
                        <span class="bold true">Estanplaza Berrini</span><br>
                        <span class="bold">Address</span>: Av. Engenheiro Luis Carlos Berrini , 853 – Brooklin Novo<br>
                        <span class="bold">Location</span>: 900 m from venue<br>
                        <a href="http://www.estanplaza.com.br/hoteis-boutique/estanplaza-berrini/resumo" target="_blank">www.estanplaza.com.br/hoteis-boutique/estanplaza-berrini/resumo</a>
                    </p>
                    <p>
                        <span class="bold true">Tryp Nações Unidas</span><br>
                        <span class="bold">Address</span>: Rua Fernandes Moreira , 1264 – Chácara Santo Antonio<br>
                        <span class="bold">Location</span>: 3 km from venue<br>
                        <a href="http://www.melia.com/pt/hoteis/brasil/sao-paulo/tryp-sao-paulo-nacoes-unidas-hotel/index.html" target="_blank">www.melia.com/pt/hoteis/brasil/sao-paulo/tryp-sao-paulo-nacoes-unidas-hotel/index.html</a>
                    </p>
                    <p>
                        <span class="bold true">Mercure São Paulo Nações Unidas</span><br>
                        <span class="bold">Address</span>: Rua Prof. Manoelito de Ornelas , 104 – Chácara Santo Antonio<br>
                        <span class="bold">Location</span>: 3,5 km from venue<br>
                        <a href="http://www.mercure.com/pt-br/hotel-3135-mercure-sao-paulo-nacoes-unidas-hotel/index.shtml" target="_blank">www.mercure.com/pt-br/hotel-3135-mercure-sao-paulo-nacoes-unidas-hotel/index.shtml</a>
                    </p>
                    <p>
                        <span class="bold true">Intercity Premium Nações Unidas</span><br>
                        <span class="bold">Address</span>: Rua Fernandes Moreira, 1371 – Chácara Santo Antonio<br>
                        <span class="bold">Location</span>: 3 km from venue<br>
                        <a href="http://www.intercityhoteis.com.br/hoteis/intercity-premium-nacoes-unidas" target="_blank">www.intercityhoteis.com.br/hoteis/intercity-premium-nacoes-unidas</a>
                    </p>

                    <h2>LEISURE ACTIVITIES & TOURISM</h2>
                    <p>For a better and pleasant stay in São Paulo, we suggest below the following touristic points to add for your experience.</p>

                    <p>
                        <strong>SHOPPING</strong><br>
                        D&D Shopping<br>
                        Shopping Cidade Jardim<br>
                        Shopping Iguatemi<br>
                        Shopping Morumbi<br>
                        Vila Madalena<br>
                        Oscar Freire
                    </p>
                    <p>
                        <strong>SPORTS</strong><br>
                        Estádio Cícero Pompeu de Toledo (Morumbi)<br>
                        Estádio Municipal Paulo Machado de Carvalho (Pacaembu)<br>
                        Jockey Club de São Paulo<br>
                        Autódromo de Interlagos
                    </p>
                    <p>
                        <strong>MUSEUMS</strong><br>
                        MASP<br>
                        Museu do Futebol<br>
                        Museu do Ipiranga<br>
                        Pinacoteca do Estado<br>
                        Museu da Língua Portuguesa
                    </p>
                    <p>
                        <strong>MONUMENTS</strong><br>
                        Ponte Estaiada Octávio Frias de Oliveira
                    </p>
                    <p>
                        <strong>BARS & RESTAURANTS</strong><br>
                        Sky<br>
                        Barbacoa<br>
                        Churrascaria Fogo de Chão<br>
                        Cervejaria Devassa
                    </p>
                    <p>
                        <strong>LEISURE & ENTERTAINMENT</strong><br>
                        Sala São Paulo<br>
                        Mercado Municipal<br>
                        Parque do Ibirapuera<br>
                        Avenida Paulista<br>
                        Liberdade
                    </p>

                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="../vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip" title="Mark this event on your calendar"></i> <i class="fa fa-calendar"></i></a> <p><strong>Date</strong><br>August 13 - 16, 2015</p>
                    </div>
                    <p class="txt">We invite you to join the largest forum in Latin America on health quality and safety together with the presentation of world known national and international speakers</p>
                    <a href="inscricao.php" class="inscrevase border">Register <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Name" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Job Title" class="border">
                                <input type="text" placeholder="Company" class="border">
                                <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> Venue & Directions</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Job Title" class="border">
                            <input type="text" placeholder="Company" class="border">
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> Contact</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Message"></textarea>
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="../images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | All rights reserved</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="../images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery.tooltipster.min.js"></script>
        <script src="../js/plugin/jquery.fancybox.js"></script>
        <script src="../js/all.js"></script>

    </body>
</html>