<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>FAQ | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="We invite you to join the largest conference in Latin America on health quality and safety together with the presentation of world known national and international speakers" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/internas.css" />
        <link rel="stylesheet" href="../css/orfao.css" />
        <link rel="stylesheet" href="../css/tooltipster.css" />
        <link rel="stylesheet" href="../css/fancybox.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body id="ingles">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="../index.php" class="port">Português</a></li>
                        <li><a href="../espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="../images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="directions.php"><i class="fa fa-map-marker"></i> <span>Venue & Directions</span></a></li>
                        <li><a href="accommodation.php"><i class="fa fa-suitcase"></i> <span>Accommodation</span></a></li>
                        <li><a href="contact.php"><i class="fa fa-envelope"></i> <span>Contact</span></a></li>
                        <li class="last select"><a href="faq.php"><i class="fa fa-question-circle"></i> <span>faq</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">about</a></li>
                        <li><a href="program.php">Program</a></li>
                        <li><a href="speakers.php">Speakers</a></li>
                        <li><a href="registration.php">Registration</a></li>
                        <li class="last"><a href="posters.php">posters</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>FAQ</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central duvidas">
                <div class="col_left">
                    <dl>
                        <dt class="bt_click" rel="#duvida01">
                            <h2>Where do I collect my certificate?</h2>
                        </dt>
                        <dd id="duvida01" class="toggleDiv">The participation certificate will be sent in an electronic format to the registered person´s e-mail during a period of 10 working days after the event. The certificate will only be issued if the enrolled participant has been present at the event.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida02">
                            <h2>Where do I collect the invoice from the registration payment?</h2>
                        </dt>
                        <dd id="duvida02" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida03">
                            <h2>May I bring an associate to the event?</h2>
                        </dt>
                        <dd id="duvida03" class="toggleDiv">All the activities during the event are restricted to participants that registered and that received their badge. Access to the venue or the permanence of non-registered associates, or people without a badge will not be allowed.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida04">
                            <h2>Will the presentations be made available at the end of the event?</h2>
                        </dt>
                        <dd id="duvida04" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida05">
                            <h2>Will it be possible to download  images of the event?</h2>
                        </dt>
                        <dd id="duvida05" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida06">
                            <h2>Does the event supply points to continuos education programs?</h2>
                        </dt>
                        <dd id="duvida06" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida07">
                            <h2>Is there any charter service to and from the venue?</h2>
                        </dt>
                        <dd id="duvida07" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida08">
                            <h2>Is there any differentiated hotel rates for the event participants?</h2>
                        </dt>
                        <dd id="duvida08" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida09">
                            <h2>Will there be wi-fi available during the event?</h2>
                        </dt>
                        <dd id="duvida09" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida10">
                            <h2>Will there be a cloakroom or coat check to keep my personal items?</h2>
                        </dt>
                        <dd id="duvida10" class="toggleDiv">Yes. This service will be available free of charge for all participants at the event venue.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida11">
                            <h2>Is filming, photographing, or audio recording allowed during the event?</h2>
                        </dt>
                        <dd id="duvida11" class="toggleDiv">Recording, filming or photographing is not allowed without the prior and express authorization of the event´s organizing committee.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida12">
                            <h2>Are first-aid services available?</h2>
                        </dt>
                        <dd id="duvida12" class="toggleDiv">The event will have at its disposal an ambulance to assist in the case of any emergency, during the entire duration of the program.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida13">
                            <h2>Is smoking allowed in the premises?</h2>
                        </dt>
                        <dd id="duvida13" class="toggleDiv">Smoking will not be allowed in the event premises.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida14">
                            <h2>What is the recommended attire?</h2>
                        </dt>
                        <dd id="duvida14" class="toggleDiv">The event does not require that participants wear special clothes. However, we recommend casual or business attire.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida15">
                            <h2>Is it possible to shift previously selected workshop options  during registration?</h2>
                        </dt>
                        <dd id="duvida15" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida16">
                            <h2>What food services have been included in registration fee?</h2>
                        </dt>
                        <dd id="duvida16" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida17">
                            <h2>What kind of food services can be found in the neighborhood?</h2>
                        </dt>
                        <dd id="duvida17" class="toggleDiv">Yes, there are two food courts next to the event venue, with over 25 options. In the surrounding areas, there are restaurants with a variety of food options.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida18">
                            <h2>When can I enroll?</h2>
                        </dt>
                        <dd id="duvida18" class="toggleDiv">For the pre-course participants, nametags or badges will be available beginning at 8:00 a.m. on August 13th. For participants for all of the other activities, badges or nametags will be made beginning at 8:00 a.m. on August 14th.</dd>
                    </dl>
                    <!--dl>
                        <dt class="bt_click" rel="#duvida19">
                            <h2>Will there be Q&A sessions with speakers?</h2>
                        </dt>
                        <dd id="duvida19" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida20">
                            <h2>Will there be time for networking?</h2>
                        </dt>
                        <dd id="duvida20" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida21">
                            <h2>Will the press have access to the event? What are the requisites for participating?</h2>
                        </dt>
                        <dd id="duvida21" class="toggleDiv">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dd>
                    </dl-->
                    <dl>
                        <dt class="bt_click" rel="#duvida22">
                            <h2>How to request for special needs?</h2>
                        </dt>
                        <dd id="duvida22" class="toggleDiv">For those who bear special needs, please send an e-mail through the "Contact" page, with a description of what you require.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida23">
                            <h2>Is there parking available at the venue?</h2>
                        </dt>
                        <dd id="duvida23" class="toggleDiv">Yes, the World Trade Center Complex has 1.700 covered parking spaces, besides its Valet service. The CENU, a complex adjacent to the WTC, has an additional 3.700 covered parking spaces, with access to the WTC Events Center. Nevertheless,  this service is not included in the enrollment amounts, and will fall exclusively upon each participant.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida24">
                            <h2>Is simultaneous interpretation being provided? Into what languages?</h2>
                        </dt>
                        <dd id="duvida24" class="toggleDiv">Yes, every activity will be translated during the event, and they can be followed in Portuguese, English, and Spanish. Headphones will be made available at the site and ID has to be provided to collect them. Your ID will be returned when you return the equipment.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida25">
                            <h2>Is there a LOST & FOUND room for lost items at the venue?</h2>
                        </dt>
                        <dd id="duvida25" class="toggleDiv">Yes, this service will be available at the event secretariat.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida26">
                            <h2>I was not able to enroll. Can I register at the venue?</h2>
                        </dt>
                        <dd id="duvida26" class="toggleDiv">There will be available room for additional enrollments, depending on the number of vacancies in each activity. Please consult the price policies for enrollment during that period in the "Registration" menu.</dd>
                    </dl>
                    <dl>
                        <dt class="bt_click" rel="#duvida27">
                            <h2>I lost my badge. How can I have access to the venue the following day?</h2>
                        </dt>
                        <dd id="duvida27" class="toggleDiv">Participants should go to the registration counter to request a copy of their nametags or badges. However, the fact that badges have been lost or forgotten may mean incurring in additional expenses for their remittance.</dd>
                    </dl-->
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="../vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip" title="Mark this event on your calendar"></i> <i class="fa fa-calendar"></i></a> <p><strong>Date</strong><br>August 13 - 16, 2015</p>
                    </div>
                    <p class="txt">We invite you to join the largest forum in Latin America on health quality and safety together with the presentation of world known national and international speakers</p>
                    <a href="inscricao.php" class="inscrevase border">Register <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Name" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Job Title" class="border">
                                <input type="text" placeholder="Company" class="border">
                                <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> Venue & Directions</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Job Title" class="border">
                            <input type="text" placeholder="Company" class="border">
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> Contact</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Message"></textarea>
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="../images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | All rights reserved</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="../images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery.tooltipster.min.js"></script>
        <script src="../js/plugin/jquery.fancybox.js"></script>
        <script src="../js/all.js"></script>
        <script src="../js/duvidas.js"></script>

    </body>
</html>