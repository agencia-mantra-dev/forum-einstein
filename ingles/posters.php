<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Posters | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="We invite you to join the largest conference in Latin America on health quality and safety together with the presentation of world known national and international speakers" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="../css/swag.css" />
        <link rel="stylesheet" href="../css/internas.css" />
        <link rel="stylesheet" href="../css/orfao.css" />
        <link rel="stylesheet" href="../css/tooltipster.css" />
        <link rel="stylesheet" href="../css/fancybox.css" />
        <link rel="stylesheet" href="../css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body id="ingles">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="../index.php" class="port">Português</a></li>
                        <li><a href="../espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="../images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="directions.php"><i class="fa fa-map-marker"></i> <span>Venue & Directions</span></a></li>
                        <li><a href="accommodation.php"><i class="fa fa-suitcase"></i> <span>Accommodation</span></a></li>
                        <li><a href="contact.php"><i class="fa fa-envelope"></i> <span>Contact</span></a></li>
                        <li class="last"><a href="faq.php"><i class="fa fa-question-circle"></i> <span>faq</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">about</a></li>
                        <li><a href="program.php">Program</a></li>
                        <li><a href="speakers.php">Speakers</a></li>
                        <li><a href="registration.php">Registration</a></li>
                        <li class="last"><a href="posters.php" class="select">posters</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>Posters</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central duvidas">
                <div class="col_left">
                    
                    <h2>Soon</h2>

                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="../vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip" title="Mark this event on your calendar"></i> <i class="fa fa-calendar"></i></a> <p><strong>Date</strong><br>August 13 - 16, 2015</p>
                    </div>
                    <p class="txt">We invite you to join the largest forum in Latin America on health quality and safety together with the presentation of world known national and international speakers</p>
                    <a href="inscricao.php" class="inscrevase border">Register <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Name" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Job Title" class="border">
                                <input type="text" placeholder="Company" class="border">
                                <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Share:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca/ingles/&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=We invite you to join the largest conference in Latin America on health quality and safety - http://goo.gl/lrLjtf&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> Venue & Directions</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> newsletter</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Job Title" class="border">
                            <input type="text" placeholder="Company" class="border">
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> Contact</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Name" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Message"></textarea>
                            <button class="border">send <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="../images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | All rights reserved</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="../images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="../js/plugin/jquery.min.js"></script>
        <script src="../js/plugin/jquery.tooltipster.min.js"></script>
        <script src="../js/plugin/jquery.fancybox.js"></script>
        <script src="../js/all.js"></script>

    </body>
</html>