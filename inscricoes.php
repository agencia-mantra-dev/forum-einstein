<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Inscrições | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/orfao.css" />
        <link rel="stylesheet" href="css/tooltipster.css" />
        <link rel="stylesheet" href="css/fancybox.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="ingles/index.php" class="eng">English</a></li>
                        <li><a href="espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> <span>como chegar</span></a></li>
                        <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> <span>hospedagem</span></a></li>
                        <li><a href="contato.php"><i class="fa fa-envelope"></i> <span>contato</span></a></li>
                        <li class="last"><a href="duvidas.php"><i class="fa fa-question-circle"></i> <span>dúvidas frequentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="evento.php">evento</a></li>
                        <li><a href="programacao.php">programação</a></li>
                        <li><a href="palestrantes.php">palestrantes</a></li>
                        <li><a href="inscricoes.php" class="select">inscrições</a></li>
                        <li class="last"><a href="trabalhos.php">trabalhos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit border">
                <div class="central">
                    <h1>inscrições</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central inscricoes">
                <div class="col_left">

                    <div id="tabs">
                        <ul class="abas">
                            <li class="item1 selected"><a href="#item1"><span>Investimento</span></a></li>
                            <li class="item2"><a href="#item2"><span>Políticas de<br>Desconto</span></a></li>
                            <li class="item3"><a href="#item3"><span>Regras<br>Cancelamento</span></a></li>
                            <li class="item4"><a href="#item4"><span>Informações</span></a></li>
                        </ul>
                        <div id="item1" class="content_aba">
                            <h2>INVESTIMENTO</h2>
                            <div class="table">
                                <span class="tit_table border">TABELA DE INSCRIÇÕES - PREÇOS POR PESSOA</span>

                                <!-- Dia 1 -->
                                <div class="esq">
                                    <span class="categ border">CATEGORIA</span>
                                    <span class="linha_simple border">Congressista</span>
                                    <span class="linha_simple border">Estudante / Func. HIAE /<br>Func. Governo</span>
                                    <span class="linha_simple border">Membros órgãos<br>apoiadores</span>
                                    <span class="linha_simple border">Visita pós-conferência<br>(16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border">1ª FASE: Até 05/04/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border">Pré-curso + Conferência<br>(3 dias: 13 a 15/08)</span>
                                        <span class="preco border">R$ 1.750</span>
                                        <span class="preco border duplo">R$ 1.313</span>
                                        <span class="preco border duplo">R$ 1.531</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border">Conferência<br>(2 dias: 14 a 15/08)</span>
                                        <span class="preco border">R$ 1.400</span>
                                        <span class="preco border duplo">R$ 1.050</span>
                                        <span class="preco border duplo">R$ 1.225</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border">Dia avulso<br>(1 dia: 13, 14 ou 15/08)</span>
                                        <span class="preco border">R$ 1.050</span>
                                        <span class="preco border duplo">R$ 788</span>
                                        <span class="preco border duplo">R$ 919</span>
                                    </span>
                                    <span class="fase border duplo none">R$ 500,00</span>
                                </div>
                                <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                                <hr><br><br><br><br><br><br>
                                <!-- Dia 2 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORIA</span>
                                    <span class="linha_simple border cinza">Congressista</span>
                                    <span class="linha_simple border cinza">Estudante / Func. HIAE /<br>Func. Governo</span>
                                    <span class="linha_simple border cinza">Membros órgãos<br>apoiadores</span>
                                    <span class="linha_simple border cinza">Visita pós-conferência<br>(16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">2ª FASE: 06/04/2015 a 07/06/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pré-curso + Conferência<br>(3 dias: 13 a 15/08)</span>
                                        <span class="preco border">R$ 2.125</span>
                                        <span class="preco border duplo">R$ 1.594</span>
                                        <span class="preco border duplo">R$ 1.859</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferência<br>(2 dias: 14 a 15/08)</span>
                                        <span class="preco border">R$ 1.700</span>
                                        <span class="preco border duplo">R$ 1.275</span>
                                        <span class="preco border duplo">R$ 1.488</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Dia avulso<br>(1 dia: 13, 14 ou 15/08)</span>
                                        <span class="preco border">R$ 1.275</span>
                                        <span class="preco border duplo">R$ 956</span>
                                        <span class="preco border duplo">R$ 1.116</span>
                                    </span>
                                    <span class="fase border duplo none">R$ 500,00</span>
                                </div>
                                <hr>
                                <!-- Dia 3 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORIA</span>
                                    <span class="linha_simple border cinza">Congressista</span>
                                    <span class="linha_simple border cinza">Estudante / Func. HIAE /<br>Func. Governo</span>
                                    <span class="linha_simple border cinza">Membros órgãos<br>apoiadores</span>
                                    <span class="linha_simple border cinza">Visita pós-conferência<br>(16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">3ª FASE: 08/06/2015 a 06/08/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pré-curso + Conferência<br>(3 dias: 13 a 15/08)</span>
                                        <span class="preco border">R$ 2.375</span>
                                        <span class="preco border duplo">R$ 1.781</span>
                                        <span class="preco border duplo">R$ 2.078</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferência<br>(2 dias: 14 a 15/08)</span>
                                        <span class="preco border">R$ 1.900</span>
                                        <span class="preco border duplo">R$ 1.425</span>
                                        <span class="preco border duplo">R$ 1.663</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Dia avulso<br>(1 dia: 13, 14 ou 15/08)</span>
                                        <span class="preco border">R$ 1.425</span>
                                        <span class="preco border duplo">R$ 1.069</span>
                                        <span class="preco border duplo">R$ 1.247</span>
                                    </span>
                                    <span class="fase border duplo none">R$ 500,00</span>
                                </div>
                                <hr>
                                <!-- Dia 4 -->
                                <div class="esq">
                                    <span class="categ border cinzao">CATEGORIA</span>
                                    <span class="linha_simple border cinza">Congressista</span>
                                    <span class="linha_simple border cinza">Estudante / Func. HIAE /<br>Func. Governo</span>
                                    <span class="linha_simple border cinza">Membros órgãos<br>apoiadores</span>
                                    <span class="linha_simple border cinza">Visita pós-conferência<br>(16/08)</span>
                                </div>
                                <div class="dir">
                                    <span class="fase border cinzao">4ª FASE (LOCAL DO EVENTO): 13/08/2015 a 16/08/2015</span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Pré-curso + Conferência<br>(3 dias: 13 a 15/08)</span>
                                        <span class="preco border">R$ 2.500</span>
                                        <span class="preco border duplo">R$ 1.875</span>
                                        <span class="preco border duplo">R$ 2.188</span>
                                    </span>
                                    <span class="col_3 meio">
                                        <span class="categ_preco border cinza">Conferência<br>(2 dias: 14 a 15/08)</span>
                                        <span class="preco border">R$ 2.000</span>
                                        <span class="preco border duplo">R$ 1.500</span>
                                        <span class="preco border duplo">R$ 1.750</span>
                                    </span>
                                    <span class="col_3">
                                        <span class="categ_preco border cinza">Dia avulso<br>(1 dia: 13, 14 ou 15/08)</span>
                                        <span class="preco border">R$ 1.500</span>
                                        <span class="preco border duplo">R$ 1.125</span>
                                        <span class="preco border duplo">R$ 1.313</span>
                                    </span>
                                    <span class="fase border duplo none">R$ 500,00</span>
                                </div>

                            </div>
                        </div>
                        <div id="item2" class="content_aba" style="display:none">
                            <h2>POLÍTICAS DE DESCONTOS E TAXAS DIFERENCIADAS</h2>
                            <p>O Fórum Anual Latino Americano de Qualidade e Segurança na Saúde oferecerá uma quantidade limitada de vagas com valores diferenciados para grupos e para categorias específicas de participantes. Esses descontos são aplicáveis ao pré-curso e à conferência e NÃO são aplicáveis às visitas monitoradas pós-conferência e demais atividades opcionais.</p>

                            <div class="tarifas">
                                <div class="tit border">Tarifas diferenciadas</div>
                                <div class="txt border">
                                    <p>Ao optar por uma das categorias abaixo, certifique-se que a instituição de ensino/ atuação foi registrada corretamente em seu cadastro. Uma cópia do comprovante deverá ser entregue no ato do credenciamento.</p>
                                    <p>A não entrega do comprovante acarretará no pagamento da diferença do valor já pago para o valor integral da inscrição.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Estudantes e residentes</div>
                                <div class="txt border">
                                    <p>Válido para estudantes de cursos de graduação, pós-graduação ou residência devidamente matriculados na data do evento.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Funcionários do Hospital Israelita Albert Einstein </div>
                                <div class="txt border">
                                    <p>Válido para funcionários registrados e membros do Corpo Clínico aberto.</p>
                                    <p>Os funcionários Einstein portadores de DRT poderão, adicionalmente, submeter solicitação para apoio financeiro.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Funcionários de órgãos de governo</div>
                                <div class="txt border">
                                    <p>Válido para funcionários de instituições públicas e órgãos de governo.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Membros de órgãos apoiadores</div>
                                <div class="txt border">
                                    <p>Válido para membros das sociedades, instituições e órgãos apoiadores do evento devidamente associados na data do evento.</p>
                                </div>
                            </div>
                            <div class="tarifas">
                                <div class="tit border">Desconto para grupos</div>
                                <div class="txt border">
                                    <p>Grupos de três ou mais pessoas de uma mesma organização são elegíveis a receber descontos sobre a tarifa de participantes individuais.</p>
                                    <p>
                                        <strong>3 a 10 pessoas</strong>: 10% de desconto<br>
                                        <strong>Acima de 10 pessoas</strong>: 20% de desconto<br><br>
                                        <strong>Inscrições de grupo</strong>: Dúvidas e solicitações devem ser encaminhadas para o e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a>.
                                    </p>
                                    <p><em>* Desconto não aplicável a funcionários Einstein quando utilizada a política de Apoio Financeiro.</em></p>
                                </div>
                            </div>
                            <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div id="item3" class="content_aba" style="display:none">
                            <h2>REGRAS DE CANCELAMENTO</h2>
                            <p>Somente serão aceitos pedidos de cancelamentos formalizados através do e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a> até o dia 24 de julho de 2015. O reembolso será de 80% do valor investido e será realizado no prazo de 30 dias a partir da data de entrega da documentação necessária. Após essa data não haverá ressarcimento.</p>

                            <h2>SUBSTITUIÇÕES</h2>
                            <p>Somente serão aceitos pedidos de substituições formalizados através do e-mail <a href="mailto:forumlatam@einstein.br" target="_blank">forumlatam@einstein.br</a> até o dia 06 de Agosto de 2015</p>
                            <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div id="item4" class="content_aba" style="display:none">
                            <h2>CERTIFICADO</h2>
                            <p>O certificado de participação será enviado em formato eletrônico para o e-mail cadastrado no ato da inscrição no prazo de 10 dias úteis após o término do evento.</p>
                            <h2>CARGA HORÁRIA</h2>
                            <p>
                                <strong>Pré-curso</strong>: 7 horas<br>
                                <strong>Conferência</strong>: 13 horas<br>
                                <strong>Visita monitorada pós-conferência</strong>: 3 horas
                            </p>

                            <h2>VAGAS</h2>
                            <p>
                                <strong>Pré-curso</strong>: 1.000<br>
                                <strong>Conferência</strong>: 2.000<br>
                                <strong>Visita pós-conferência</strong>: 50 por módulo
                            </p>

                            <h2>FORMAS DE PAGAMENTO</h2>
                            <p>Em breve</p>

                            <h2>IDENTIFICAÇÃO</h2>
                            <p>A credencial de acesso ao evento é pessoal e intransferível. Seu uso é obrigatório em todas as atividades, tanto para acesso como para permanência nas dependências do evento. Esquecimento ou perda da credencial poderá acarretar em um custo adicional para emissão da 2ª via.</p>

                            <h2>COMPROVANTE DE PAGAMENTO</h2>
                            <p>O comprovante fiscal referente ao pagamento da taxa de inscrição será enviado para o e-mail cadastrado no ato da inscrição no prazo de 7 dias úteis após o término do evento.</p>

                            <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip"  title="Marque este evento no seu calendário"></i> <i class="fa fa-calendar"></i></a> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais.</p>
                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery.tooltipster.min.js"></script>
        <script src="js/plugin/jquery.fancybox.js"></script>
        <script src="js/all.js"></script>
        <script src="js/inscricoes.js"></script>

    </body>
</html>