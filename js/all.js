$(document).ready(function () {

    //Placeholder
    $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });

    //Menu mobile
    $(".mn_button").click(function () {
        $(".holder_menu").toggleClass("open");
        if ($('.holder_menu').hasClass('open')) {
            $(".mn_button i").addClass("fa-close");
        }else{
            $(".mn_button i").removeClass("fa-close");
        }
    });

    //Tooltip
    $(".tooltip").tooltipster();

    //Avise-me
    /*$(".inscrevase").fancybox({
        'width'           : 450,
        'height'          : 450,
        'padding'         : '15px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
    });*/

});