$( document ).ready(function() {
    //Banners home
    $(".banners").each(function(index) {
        $(this).cycle({
            fx:     'scrollHorz',
            pager:  '#nav',
            timeout: 7000,
            pagerAnchorBuilder: function(i) {
                if (index == 0)
                // for first slideshow, return a new anchro
                return '<a href="#">'+(i+1)+'</a>';
                // for 2nd slideshow, select the anchor created previously
                return '#nav a:eq('+i+')';
            }
        });
    });

    //Hover imagens home
    $(".galeria li")
    .stop().mouseenter(function(){
        $(this).find("span").stop().fadeIn();
    }).stop().mouseleave(function(){
        $(this).find("span").stop().fadeOut();
    });

    //Modais Home
    $(".galeria li a").fancybox({
        'centerOnScroll'  : true,
        'autoScale'       : true
    });

    //Avise-me
    /*$(".botao a").fancybox({
        'width'           : 450,
        'height'          : 450,
        'padding'         : '15px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
    });*/

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if( isMobile.any() ){
        $(".mobile").each(function(index) {
            $(this).cycle({
                fx:     'scrollHorz',
                pager:  '#nav',
                timeout: 7000,
                pagerAnchorBuilder: function(i) {
                    if (index == 0)
                    // for first slideshow, return a new anchro
                    return '<a href="#">'+(i+1)+'</a>';
                    // for 2nd slideshow, select the anchor created previously
                    return '#nav a:eq('+i+')';
                }
            });
        });
    };

});