$( document ).ready(function() {
    $(".item a")
    .stop().mouseenter(function(){
        $(this).find("span").stop().fadeIn();
    }).stop().mouseleave(function(){
        $(this).find("span").stop().fadeOut();
    });
    $(".modal").fancybox({
        'width'           : 710,
        'height'          : 350,
        'padding'         : '55px',
        'centerOnScroll'  : true,
        'autoScale'       : true
    });
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if( isMobile.any() ){
        $(".modal").fancybox({
        'width'           : 710,
        'height'          : '100%',
        'padding'         : '15px',
        'centerOnScroll'  : true,
        'autoScale'       : true
    });
    };
});