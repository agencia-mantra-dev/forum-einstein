<% @CodePage = 65001 %>
<% Response.CodePage = 65001 %>
<%
Dim objCDOSYSMail
Dim objCDOSYSCon

'CRIA A INSTANCIA COM O OBJETO CDOSYS
Set objCDOSYSMail = Server.CreateObject("CDO.Message")

'CRIA A INSTANCIA DO OBJETO PARA CONFIGURACAO DO SMTP
Set objCDOSYSCon = Server.CreateObject ("CDO.Configuration")

'SERVIDOR DE SMTP, USE mail.SeuDominio.com
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "MAISV4"

'PORTA PARA COMUNICACAO COM O SERVIÇO DE SMTP
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25

'PORTA DO CDO
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

'ATIVAR RECURSO DE SMTP AUTENTICADO
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1

'USUARIO PARA SMTP AUTENTICADO
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = "domhiae\_ldpadm"

'SENHA DO USUARIO PARA SMTP AUTENTICADO
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "*B))K$@))$"

'TEMPO DE TIMEOUT (EM SEGUNDOS)
objCDOSYSCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30

'ATUALIZA A CONFIGURACAO DO CDOSYS PARA ENVIO DO E-MAIL
objCDOSYSCon.Fields.update
Set objCDOSYSMail.Configuration = objCDOSYSCon

'NOME E E-MAIL DO REMETENTE
objCDOSYSMail.From = "Inscrições em grupo - 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade <forumlatam@einstein.br>"
'"Nome do Remetente <lucas.bonifacio@einstein.br>"

'NOME E E-MAIL DO DESINATARIO
objCDOSYSMail.To = "michelle.benedicto@einstein.br;forumlatam@einstein.br"

'CONFIGURA O E-MAIL QUE RECEBERA A RESPOSTA DESTA MENSAGEM
'objCDOSYSMail.ReplyTo = "Nome <email@seudominio.com.br>"

'ASSUNTO DA MENSAGEM
objCDOSYSMail.Subject = "Inscrições em grupo - 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade"

responsavel = request.form("nome")
cpfResponsavel = request.form("cpf")
emailResponsavel = request.form("email")
telefoneResponsavel = request.form("telefone")
empresa = request.form("empresa")
categoria = request.form("categoria")
participantes = split(request.form("participantes")," / ")
numParticipantes = ubound(participantes)

participantesTabela = ""
for i = 0 to ubound(participantes)
	participanteLoop = split(participantes(i),";")
	for j = 0 to ubound(participanteLoop)
		if j = 0 then
		participantesTabela = participantesTabela + "<tr><td align='center' width='20%' style='border-bottom:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & participanteLoop(j) & "</p></td>"
		elseif j = 2 then
		participantesTabela = participantesTabela & "<td align='center' width='20%' style='border-bottom:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><a href='mailto:" & participanteLoop(j) & "' style='color:#00b3e3;'>" & participanteLoop(j) & "</a></p></td>"
		elseif j = ubound(participanteLoop) + 1 then
		participantesTabela = participantesTabela + "</tr>"
		else
		participantesTabela = participantesTabela & "<td align='center' width='20%' style='border-bottom:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & participanteLoop(j) & "</p></td>"
		end if
	next
next

'CONTEÚDO DA MENSAGEM
objCDOSYSMail.HtmlBody = "<html><head><meta charset='utf-8'><title>Inscrições em grupo - 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title></head><body><table width='700' cellpadding='0' cellspacing='0' align='center' style='border:1px solid #e2e7ea;'> <tr> <td colspan='3' height='20'></td></tr><tr> <td width='20' style='border-bottom:1px solid #e2e7ea;'>&nbsp;</td><td style='border-bottom:1px solid #e2e7ea;padding:10px 0;'><h1 style='color:#00b3e3;font-family:Arial, Helvetica, sans-serif;font-size:18px; font-weight:normal;text-align:center;'>1º Fórum Latino Americano de<br/> <strong>Qualidade e Segurança na Saúde</strong><br/> Em busca da sustentabilidade</h1></td><td width='20' style='border-bottom:1px solid #e2e7ea;'>&nbsp;</td></tr><tr> <td colspan='3' height='20'></td></tr><tr> <td width='20'>&nbsp;</td><td> <h2 style='text-align:center;font-family:Arial, Helvetica, sans-serif; color:#00b3e3;font-size:18px;'>Inscrição em grupo</h2> <p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:16px;line-height:20px;'><strong>Responsável pelo grupo</strong></p><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #e2e7ea;'> <tr> <td width='20%' style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><strong>Nome</strong></p></td><td style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & responsavel & "</p></td></tr><tr> <td width='20%' style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><strong>CPF</strong></p></td><td style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & cpfResponsavel & "</p></td></tr><tr> <td width='20%' style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><strong>E-mail</strong></p></td><td style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><a href='mailto:" & emailResponsavel & "' style='color:#00b3e3;'>" & emailResponsavel & "</a></p></td></tr><tr> <td width='20%' style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><strong>Telefone</strong></p></td><td style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & telefoneResponsavel & "</p></td></tr><tr> <td width='20%' style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'><strong>Empresa</strong></p></td><td style='border-top:1px dotted #e2e7ea;padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & empresa & "</p></td></tr></table> <br/> <p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:16px;line-height:20px;'><strong>Categoria de participação</strong></p><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #e2e7ea;padding:10px 0;'> <tr> <td><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>" & categoria & "</p></td></tr></table> <br/> <p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:16px;line-height:20px;'><strong>Participantes cadastrados</strong></p><table cellpadding='0' cellspacing='0' width='100%' style='border-top:1px solid #e2e7ea;'> <thead> <tr> <th style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>Nome</p></th> <th style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>CPF</p></th> <th style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>E-mail</p></th> <th style='padding:10px 0;'><p style='font-family:Arial, Helvetica, sans-serif;color:#6d6e71;font-size:14px;line-height:20px;'>Pacote</p></th> </tr></thead> <tbody>" & participantesTabela & "</tbody> </table> <br/> </td><td width='20'>&nbsp;</td></tr><tr> <td colspan='3' height='20'></td></tr></table></body></html>"

'PARA ENVIO DA MENSAGEM NO FORMATO HTML, ALTERE O TextBody PARA HtmlBody
'objCDOSYSMail.HtmlBody = "Digite aqui sua mensagem"

objCDOSYSMail.HTMLBodyPart.charset = "utf-8"
objCDOSYSMail.BodyPart.charset = "utf-8" 

'ENVIA A MENSAGEM
objCDOSYSMail.Send

'DESTRÓI OS OBJETOS
Set objCDOSYSMail = Nothing
Set objCDOSYSCon = Nothing

'response.write "Seu e-mail foi enviado. Obrigado pelo contato!"
%>