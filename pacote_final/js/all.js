$(document).ready(function () {

    //Placeholder
	$('input:text').each(function(){
        var txtval = $(this).val();
        $(this).focus(function(){
            if($(this).val() == txtval){
                $(this).val('');
            }
        });
        $(this).blur(function(){
            if($(this).val() == ""){
                $(this).val(txtval);
            }
        });
    });

    //Menu mobile
    $(".mn_button").click(function () {
        $(".holder_menu").toggleClass("open");
        if ($('.holder_menu').hasClass('open')) {
            $(".mn_button i").addClass("fa-close");
        }else{
            $(".mn_button i").removeClass("fa-close");
        }
    });

    //Tooltip
    $(".tooltip").tooltipster();

    //Avise-me
    /*$(".inscrevase").fancybox({
        'width'           : 600,
        'height'          : 650,
        'padding'         : '15px',
        'centerOnScroll'  : true,
        'autoScale'       : false,
		'autoSize' : false,
		'autoDimensions' : false
    });*/
});