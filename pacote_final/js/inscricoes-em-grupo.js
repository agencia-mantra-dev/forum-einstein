// JavaScript Document
$(".cont-form input").focus(function(){
	var temAviso = false;
	if($(this).hasClass("vermelho")){
		temAviso = true;
	}	
	$(".cont-form .aviso").text("").hide().removeClass("vermelho");
	var campo = $(this).attr("name");	
	switch(campo){
		case "5281":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o nome do responsável pelo grupo").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("Nome").fadeIn("slow");
			}		
		break;
		case "5286":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o CPF do responsável pelo grupo").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("CPF").fadeIn("slow");
			}		
		break;
		case "1156":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o e-mail do responsável pelo grupo").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("E-mail").fadeIn("slow");
			}		
		break;
		case "3410":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o telefone do responsável pelo grupo").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("Telefone").fadeIn("slow");
			}		
		break;
		case "3405":
		$(this).prev(".aviso").text("Empresa").fadeIn("slow");		
		break;
		case "5283":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o nome do participante").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("Nome").fadeIn("slow");
			}		
		break;
		case "4104":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha um CPF válido").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("CPF").fadeIn("slow");
			}		
		break;					
		case "3179":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o e-mail corretamente").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("E-mail").fadeIn("slow");
			}		
		break;
		case "3178":
			if(temAviso){
			$(this).prev(".aviso").text("Por favor, preencha o telefone corretamente").fadeIn('slow').addClass("vermelho");
			}else{
			$(this).prev(".aviso").text("Telefone").fadeIn("slow");
			}		
		break;							
	}
});

$("#representanteGrupo label").click(function(){
	if($(".ireiParticipar").is(":checked")){
		$(".ireiParticipar").attr("checked",false);
		$("#repPacotes").fadeOut("slow");
	}else{
		$(".ireiParticipar").attr("checked",true);
		$("#repPacotes").fadeIn("slow");
	}	
});
$(".ireiParticipar").change(function(){
	if($(this).is(":checked")){
		$("#repPacotes").fadeIn("slow");
	}else{		
		$("#repPacotes").fadeOut("slow");
	}
});


$("#bt276").click(function(){
	var nome = $(".formGrupo #c5281 input").val(),
	cpf = $("#c5286 input").val().replace(/([.*+?^=!:${}()|\[\]\/\\-])/g, ""),
	cpfFormat = $("#c5286 input").val(),
	email = $(".formGrupo #c1156 input").val(),
	telefone = $(".formGrupo #c3410 input").val(),
	empresa = $(".formGrupo #c3405 input").val(),
	participantes = "",
	categoriaId = $(".formGrupo #c5288 select").val(),
	categoriaNome = $(".formGrupo #c5288 select option:selected").text(),
	filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
	numParticipantes = $("#listaParticipantes table tr").length;
	
	if(numParticipantes < 3){
		participantes = "false";
	}else{
		$("#listaParticipantes .linhaParticipante").each(function(){
			var nomeParticipante = $(this).find(".5283").text(),
			cpfParticipante = $(this).find(".4104").text(),
			emailParticipante = $(this).find(".3179").text(),
			pacoteParticipante;
			
			switch($(this).find(".pacoteEscolhido").text()){
				case "13, 14 e 15/08":
				pacoteParticipante = "Pré-Conferência + Conferência - 13, 14 e 15/08";
				break;
				
				case "14 e 15/08":
				pacoteParticipante = "Conferência - 14 e 15/08";
				break;
				
				case "13/08":
				pacoteParticipante = "Dia avulso - 13/08";
				break;
				
				case "14/08":
				pacoteParticipante = "Dia avulso - 14/08"
				break;	
				
				case "15/08":
				pacoteParticipante = "Dia avulso - 15/08"
				break;																
			}
			
			
			if(numParticipantes == 1 || $(this).index() == 0){
				participantes = nomeParticipante + ";" + cpfParticipante + ";" + emailParticipante + ";" + pacoteParticipante;
			}else{
				participantes = participantes + " / " + nomeParticipante + ";" + cpfParticipante + ";" + emailParticipante + ";" + pacoteParticipante;
			}			
		});	
	}
	
	if(nome.length < 2 || nome == "Nome"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5281 input").focus().select().addClass("vermelho");
		$("#c5281 .aviso").text("Por favor, preencha o nome do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(testaCPF(cpf) == false){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5286 input").focus().select().addClass("vermelho");
		$(".formGrupo #c5286 .aviso").text("Por favor, preencha o CPF do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(!filter.test(email)){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");	
		$(".formGrupo #c1156 input").focus().select().addClass("vermelho");
		$(".formGrupo #c1156 .aviso").text("Por favor, preencha o e-mail do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(telefone.length < 10 || telefone == "Telefone"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c3410 input").focus().select().addClass("vermelho");
		$(".formGrupo #c3410 .aviso").text("Por favor, preencha o telefone do responsável pelo grupo").fadeIn('slow').addClass("vermelho");
	}else if(categoriaId == "" || categoriaNome == "Categoria de participação"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5288 select").addClass("vermelho");
		$(".formGrupo #c5288 .aviso").text("Por favor, escolha a categoria de participação").fadeIn("slow").addClass("vermelho");
	}else if(participantes == "" || participantes == "false"){
		if(participantes == "false"){
			alert("O número mínimo de participantes a serem cadastrados é 3. Por favor, inclua mais participantes.");
			$('html, body').animate({scrollTop : $("#formParticipantes").offset().top}, 500);
		}else{
			$(".formGrupo .aviso").hide().removeClass("vermelho");
			$(".formGrupo input, .formGrupo select").removeClass("vermelho");
			$(".formGrupo #formParticipantes input").addClass("vermelho");
			$(".formGrupo #c5283").focus().select();
			$(".formGrupo #c5283 .aviso").text("Por favor, preencha o nome do participante").fadeIn('slow').addClass("vermelho");
			$(".formGrupo #c4104 .aviso").text("Por favor, preencha um CPF válido").fadeIn('slow').addClass("vermelho");
			$(".formGrupo #c3179 .aviso").text("Por favor, preencha o e-mail corretamente").fadeIn('slow').addClass("vermelho");
			$(".formGrupo #c3178 .aviso").text("Por favor, preencha o telefone corretamente").fadeIn('slow').addClass("vermelho");		
		}			
	}else{
		enviaEmail();
		/*var formData = "5281=" + nome + "&1156=" + email + "&3410=" + telefone + "&3405=" + empresa + "&5283=" + participantes + "&5286=" + cpfFormat;		
		$.ajax({
			url:'http://apps.einstein.br/ensino/qued/submit-qued3.asp?action=I',
			type:'POST',
			data:formData,
			success: enviaEmail()
		});*/		
		function enviaEmail(){
			$.post("enviaemail.asp", {nome:nome,cpf:cpfFormat,email:email,telefone:telefone,empresa:empresa, categoria:categoriaNome, participantes:participantes})
			.done(function(data){
				$(".formGrupo #qued-form").hide();
				$(".formGrupo").append('<p class="aviso verde">Sua solicitação foi enviada com sucesso!<br />Em breve entraremos em contato com as instruções para o cadastro e pagamento das inscrições.</p><a onclick="location.reload()" class="novoGrupo inscrevase border">Inscrever novo grupo <i class="fa fa-arrow-circle-right"></i></a>');
				$('html, body').animate({scrollTop : $("#formularioGrupo").offset().top}, 500);
			});
		}	
	}
	
});

//Participantes
var indexEditado;
$(".formGrupo #c4104 input, .formGrupo #c5286 input").focus(function(){
	$(this).mask('000.000.000-00', {reverse: true});
});

$(".formGrupo #c3178 input, #c3410 input").focus(function(){
	if($(this).val().length <= 14){
		$(this).mask('(00) 0000-0000');		
	}else{
		$(this).mask("(00) 00000-0000");
	}	
	$(this).keypress(function(){
		var comprimento = $(this).val().length;
		if(comprimento == 14){
			$(this).mask("(00) 00000-0000");
		}else{
			$(this).mask("(00) 0000-0000");
		}
	});
});

$(".adicionarParticipante").click(function(){
	var nome = $("#c5283 input").val(),
	cpf = $("#c4104 input").val().replace(/([.*+?^=!:${}()|\[\]\/\\-])/g, ""),
	cpfFormat = $("#c4104 input").val(),
	email = $("#c3179 input").val(),	
	pacote = $(".formGrupo #escolhaPacote").val(),
	pacoteData = $(".formGrupo #escolhaPacote option:selected").val(),
	pacoteRep = $("#repPacotes select").val(),
	filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
	repNome = $("#c5281 input").val(),
	repCpf = $("#c5286 input").val(),
	repEmail = $("#c1156 input").val(),
	repTelefone = $("#c3410 input").val(),
	repEmpresa = $(".formGrupo #c3405 input").val(),
	repPacote = $("#repPacotes select option:selected").val();
			
	if(repNome.length < 2 || repNome == "Nome"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5281 input").focus().select().addClass("vermelho");
		$("#c5281 .aviso").text("Por favor, preencha o nome do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(testaCPF($("#c5286 input").val().replace(/([.*+?^=!:${}()|\[\]\/\\-])/g, "")) == false){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5286 input").focus().select().addClass("vermelho");
		$(".formGrupo #c5286 .aviso").text("Por favor, preencha o CPF do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(!filter.test(repEmail)){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");	
		$(".formGrupo #c1156 input").focus().select().addClass("vermelho");
		$(".formGrupo #c1156 .aviso").text("Por favor, preencha o e-mail do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if(repTelefone.length < 10 || repTelefone == "Telefone"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c3410 input").focus().select().addClass("vermelho");
		$(".formGrupo #c3410 .aviso").text("Por favor, preencha o telefone do responsável pelo grupo").fadeIn('slow').addClass("vermelho");		
	}else if($(".ireiParticipar").is(":checked") && pacoteRep == "Pacote de participação"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$("#repPacotes select").addClass("vermelho");
		$("#repPacotes .aviso").text("Por favor, escolha o pacote de participação do responsável").fadeIn('slow').addClass("vermelho");			
	}else if(nome.length < 2 || nome == "Nome"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");
		$(".formGrupo #c5283 input").focus().select().addClass("vermelho");
		$(".formGrupo #c5283 .aviso").text("Por favor, preencha o nome do participante").fadeIn('slow').addClass("vermelho");
	}else if(testaCPF(cpf) == false){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");		
		$(".formGrupo #c4104 input").focus().select().addClass("vermelho");
		$(".formGrupo #c4104 .aviso").text("Por favor, preencha um CPF válido").fadeIn('slow').addClass("vermelho");
	}else if(!filter.test(email)){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");		
		$(".formGrupo #c3179 input").focus().select().addClass("vermelho");
		$(".formGrupo #c3179 .aviso").text("Por favor, preencha o e-mail corretamente").fadeIn('slow').addClass("vermelho");
	}else if(pacote == "" || pacote == "Pacote de participação"){
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo #escolhaPacote").removeClass("vermelho");		
		$(".formGrupo #escolhaPacote").addClass("vermelho");
		$(".formGrupo #pacotes .aviso").text("Por favor, escolha o pacote de participação").fadeIn('slow').addClass("vermelho");
	}else{
		$(".formGrupo .aviso").hide().removeClass("vermelho");
		$(".formGrupo input, .formGrupo select").removeClass("vermelho");			
			
		
		if($("#listaParticipantes").is(":hidden")){
			$("#listaParticipantes").show();
		}
		
		if($(".ireiParticipar").is(":checked")){			
			if($("#listaParticipantes table tr").eq(0).hasClass("responsavel")){
				$("#listaParticipantes table tr").eq(0).remove();
				$("#listaParticipantes table.participantes").prepend('<tr class="linhaParticipante responsavel"> <td width="20%" class="5283" formResponsavel="5281">' + repNome + '</td><td width="20%" class="4104" formResponsavel="5286">' + repCpf + '</td><td width="20%" class="3179" formResponsavel="1156">' + repEmail + '</td><td width="20%" class="pacoteEscolhido" formResponsavel="representanteGrupo">' + repPacote + '</td><td width="20%"><a class="editarParticipante border acoesParticipantes" onclick="$(this).editarParticipante();" href="#formParticipantes">Editar</a></td></tr>');
			}else{
				$("#listaParticipantes table.participantes").prepend('<tr class="linhaParticipante responsavel"> <td width="20%" class="5283" formResponsavel="5281">' + repNome + '</td><td width="20%" class="4104" formResponsavel="5286">' + repCpf + '</td><td width="20%" class="3179" formResponsavel="1156">' + repEmail + '</td><td width="20%" class="pacoteEscolhido" formResponsavel="representanteGrupo">' + repPacote + '</td><td width="20%"><a class="editarParticipante border acoesParticipantes" onclick="$(this).editarParticipante();" href="#formParticipantes">Editar</a></td></tr>');
			}	
			
		}
		
		$("#listaParticipantes table.participantes").append('<tr class="linhaParticipante"> <td width="20%" class="5283">' + nome + '</td><td width="20%" class="4104">' + cpfFormat + '</td><td width="20%" class="3179">' + email + '</td><td width="20%" class="pacoteEscolhido">' + pacoteData + '</td><td width="20%"><a class="editarParticipante border acoesParticipantes" onclick="$(this).editarParticipante();" href="#formParticipantes">Editar</a><a class="removerParticipante border acoesParticipantes" onclick="$(this).removerParticipante();">Remover</a></td></tr>');
		
		$(".formGrupo #c5283 input").val("Nome");
		$(".formGrupo #c4104 input").val("CPF");
		$(".formGrupo #c3179 input").val("E-mail");
		$(".formGrupo #c3178 input").val("Telefone");
		$("#escolhaPacote").val("Pacote de participação");
		
		$('html, body').animate({scrollTop : $("#listaParticipantes").offset().top}, 500);		
	}
});

$(".incluirParticipante").click(function(){
	$('html, body').animate({scrollTop : $("#formParticipantes").offset().top}, 500);
	$(".formGrupo #c5283 input").focus().select();
});

$.fn.removerParticipante = function(){
	$(this).parent().parent().remove();
	if($("#listaParticipantes table tr").length == 0){
		$("#listaParticipantes").hide();
	}
}

$.fn.editarParticipante = function(){	
	$('html, body').animate({scrollTop : $("#formParticipantes").offset().top}, 500);
	var index = $(this).parent().parent(".linhaParticipante").index();
	indexEditado = index;
	$(".adicionarParticipante").hide();
	$(".salvarParticipante").show();
	
	$("#listaParticipantes table tr").eq(index).find("td").each(function(){
		var classe = $(this).attr('class');
		if(classe != "pacoteEscolhido"){
			$(".formGrupo #c" + classe + " input").val($(this).text());
		}else{
			$(".formGrupo #escolhaPacote").val($(this).text());
		}

		$(".formGrupo #c5283 input").focus().select();
	});
} 

$(".salvarParticipante").click(function(){
	if($("#listaParticipantes table tr").eq(indexEditado).hasClass("responsavel")){
		$("#listaParticipantes table tr").eq(indexEditado).find("td").each(function(){
			var attr = $(this).attr("formResponsavel"),
			classe = $(this).attr("class");
			if(attr != "representanteGrupo"){
				$("#c" + attr + " input").val($("#c" + classe + " input").val());
			}else{
				$("#escolhaPacoteRep").val($("#escolhaPacote").val());
			}			
		});
	}
	$("#listaParticipantes table tr").eq(indexEditado).find("td").each(function(){
		var classe = $(this).attr("class");
		$("#listaParticipantes tr").eq(indexEditado).find("." + classe).text($("#c" + classe + " input").val());				
	});	
		$("#c5283 input").val("Nome");
		$("#c4104 input").val("CPF");
		$("#c3179 input").val("E-mail");
		$("#escolhaPacote").val("Pacote de participação");
			
	$(this).hide();
	$(".adicionarParticipante").show();	
	$('html, body').animate({scrollTop : $("#listaParticipantes").offset().top}, 500);
});

function testaCPF(strCPF){ 
	var Soma; 
	var Resto; 
	Soma = 0; 
	if (strCPF == "00000000000") return false;	
	for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);	
	Resto = (Soma * 10) % 11; 	
	if ((Resto == 10) || (Resto == 11)) Resto = 0; 	
	if (Resto != parseInt(strCPF.substring(9, 10)) ) return false; 	
	Soma = 0; for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i); 	
	Resto = (Soma * 10) % 11; if ((Resto == 10) || (Resto == 11)) Resto = 0; 	
	if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false; return true; 
}