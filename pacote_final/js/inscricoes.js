var setPublico, setPacote, setDia;
//Checa para ver se é a página de escolha do Apoio Financeiro
if(typeof apoioSet !== 'undefined' && apoioSet == true){
	setPublico = publicoSet;
	setPacote = pacoteSet;
	setDia = diaSet;
}
var escolheuApoio = false;
var publico = {
    congressitas: "congressitas",
    estudantes: "estudantes",
    menbros: "menbros",
    cortesia: "cortesia"
};

var pacote = {
    avulso: "avulso",
    conferencia: "conferencia",
    completo: "completo",
    apoiofinanceiro: "apoiofinanceiro"
};

// Todas as variações de perfil
var  tabelaProdutos = {};

// publico + dia 
tabelaProdutos["congressitas+13"] = { Id : "841" };
tabelaProdutos["estudantes+13"] = { Id : "842" };
tabelaProdutos["menbros+13"] = { Id : "844" };
tabelaProdutos["cortesia+13"] = { Id : "841" };

tabelaProdutos["congressitas+14"] = { Id : "846" };
tabelaProdutos["estudantes+14"] = { Id : "847" };
tabelaProdutos["menbros+14"] = { Id : "848" };
tabelaProdutos["cortesia+14"] = { Id : "846" };

tabelaProdutos["congressitas+15"] = { Id : "849" };
tabelaProdutos["estudantes+15"] = { Id : "850" };
tabelaProdutos["menbros+15"] = { Id : "851" };
tabelaProdutos["cortesia+15"] = { Id : "849" };

tabelaProdutos["congressitas+1415"] = { Id : "872" };
tabelaProdutos["estudantes+1415"] = { Id : "873" };
tabelaProdutos["menbros+1415"] = { Id : "875" };
tabelaProdutos["cortesia+1415"] = { Id : "872" };

tabelaProdutos["congressitas+131415"] = { Id : "869" };
tabelaProdutos["estudantes+131415"] = { Id : "870" };
tabelaProdutos["menbros+131415"] = { Id : "871" };
tabelaProdutos["cortesia+131415"] = { Id : "869" };


//Validação especial
var tabelaPerfilCongressitaDia13 = {};
var tabelaPerfilCongressitaDia14 = {};
var tabelaPerfilCongressitaDia15 = {};
var tabelaPerfilCongressitaDia1415 = {};
var tabelaPerfilCongressitaDia131415 = {};

var tabelaPerfilEstudanteDia13 = {};
var tabelaPerfilEstudanteDia14 = {};
var tabelaPerfilEstudanteDia15 = {};
var tabelaPerfilEstudanteDia1415 = {};
var tabelaPerfilEstudanteDia131415 = {};

var tabelaPerfilMembrosDia13 = {};
var tabelaPerfilMembrosDia14 = {};
var tabelaPerfilMembrosDia15 = {};
var tabelaPerfilMembrosDia1415 = {};
var tabelaPerfilMembrosDia131415 = {};

tabelaPerfilCongressitaDia13["841"] = { temProduto : false, reqOk : false }; // CONGRESSITA
tabelaPerfilCongressitaDia14["846"] = { temProduto : false, reqOk : false }; // CONGRESSITA
tabelaPerfilCongressitaDia15["849"] = { temProduto : false, reqOk : false }; // CONGRESSITA
tabelaPerfilCongressitaDia1415["872"] = { temProduto : false, reqOk : false }; // CONGRESSITA
tabelaPerfilCongressitaDia131415["869"] = { temProduto : false, reqOk : false }; // CONGRESSITA

tabelaPerfilEstudanteDia13["842"] = { temProduto : false, reqOk : false }; // ESTUDANTES FUNCIONARIOS...
tabelaPerfilEstudanteDia14["847"] = { temProduto : false, reqOk : false }; // ESTUDANTES FUNCIONARIOS...
tabelaPerfilEstudanteDia15["850"] = { temProduto : false, reqOk : false }; // ESTUDANTES FUNCIONARIOS...
tabelaPerfilEstudanteDia1415["873"] = { temProduto : false, reqOk : false }; // ESTUDANTES FUNCIONARIOS...
tabelaPerfilEstudanteDia131415["870"] = { temProduto : false, reqOk : false }; // ESTUDANTES FUNCIONARIOS...

tabelaPerfilMembrosDia13["844"] = { temProduto : false, reqOk : false }; // MEMBROS E ÓRGÃOS
tabelaPerfilMembrosDia14["848"] = { temProduto : false, reqOk : false }; // MEMBROS E ÓRGÃOS
tabelaPerfilMembrosDia15["851"] = { temProduto : false, reqOk : false }; // MEMBROS E ÓRGÃOS
tabelaPerfilMembrosDia1415["875"] = { temProduto : false, reqOk : false }; // MEMBROS E ÓRGÃOS
tabelaPerfilMembrosDia131415["871"] = { temProduto : false, reqOk : false }; // MEMBROS E ÓRGÃOS

var  tabelaProdutosDia13 = {};
// Manhã
tabelaProdutosDia13["853"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["854"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["855"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["852"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["856"] = { temProduto : false, reqOk : false };
// Tarde
tabelaProdutosDia13["857"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["858"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["859"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["860"] = { temProduto : false, reqOk : false };
tabelaProdutosDia13["861"] = { temProduto : false, reqOk : false };

var  tabelaProdutosDia14 = {};
// Manhã
tabelaProdutosDia14["862"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["863"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["864"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["865"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["866"] = { temProduto : false, reqOk : false };
// Tarde
tabelaProdutosDia14["867"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["868"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["883"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["884"] = { temProduto : false, reqOk : false };
tabelaProdutosDia14["885"] = { temProduto : false, reqOk : false };

var  tabelaProdutosDia15 = {};
// Manhã
tabelaProdutosDia15["890"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["892"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["886"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["888"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["894"] = { temProduto : false, reqOk : false };
// Tar
tabelaProdutosDia15["891"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["893"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["887"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["889"] = { temProduto : false, reqOk : false };
tabelaProdutosDia15["895"] = { temProduto : false, reqOk : false };

var count = 0;
var count1 = 0;
var clickedPublico = false;

// Variaveis que determinão se ja termino todos os produtos do dia 13, 14 e 15.
var allReqFinish = false;

$(document).ready(function () {

    $(".publico").find(".seta").hide();

    var VerificaPacotesDoDia = function(){

        // Aqui vai rola o script para com a logica solicitada.
        // Verifico se tenho todos os produtos dos ua 13, 14 e 15.

        // Variaveis que determinão se eu tenho todos os produtos do dia 13, 14 e 15.
        var NaoTenhoDia13 = false;
        var NaoTenhoDia14 = false;
        var NaoTenhoDia15 = false;
        var NaoTenhoDia1415 = false;
        var NaoTenhoDia131415 = false;

        //{ temProduto = false; reqOk = false }
        //Se eu tuver algum "true", significa que um ou mais produto encontra-se esgotado para tal evento.
        if (tabelaPerfilCongressitaDia13[841].temProduto || 
            tabelaPerfilEstudanteDia13[842].temProduto ||
            tabelaPerfilMembrosDia13[844].temProduto)
        {
            NaoTenhoDia13 = true;
        };

        if (tabelaPerfilCongressitaDia14[846].temProduto ||
            tabelaPerfilEstudanteDia14[847].temProduto ||
            tabelaPerfilMembrosDia14[848].temProduto) 
        {
            NaoTenhoDia14 = true;
        };

        if (tabelaPerfilCongressitaDia15[849].temProduto ||
            tabelaPerfilEstudanteDia15[850].temProduto ||
            tabelaPerfilMembrosDia15[851].temProduto) 
        {
            NaoTenhoDia15 = true;
        };

        if (tabelaPerfilCongressitaDia1415[872].temProduto ||
            tabelaPerfilEstudanteDia1415[873].temProduto ||
            tabelaPerfilMembrosDia1415[875].temProduto) 
        {
            NaoTenhoDia1415 = true;
        };

        if (tabelaPerfilCongressitaDia131415[869].temProduto ||
            tabelaPerfilEstudanteDia131415[870].temProduto ||
            tabelaPerfilMembrosDia131415[871].temProduto) 
        {
            NaoTenhoDia131415 = true;
        };

        // Debug
        // situação 1 não tem vaga dia 13
        //NaoTenhoDia13 = true;
        //NaoTenhoDia14 = true;
        //NaoTenhoDia15 = true;
        //NaoTenhoDia1415 = true;
        //NaoTenhoDia131415 = true;

        if (NaoTenhoDia13) {
            $(".pacote[dia=13]").addClass("disabled");
        };
        if (NaoTenhoDia14) {
            $(".pacote[dia=14]").addClass("disabled");
        };
        if (NaoTenhoDia15) {
            $(".pacote[dia=15]").addClass("disabled");
        };
        if (NaoTenhoDia1415) {
             //- bloqueia o pacote conferência
            $(".dois_dias").addClass("disabled");
            $(".pacote[pacote=conferencia]").addClass("disabled");
        };
        if (NaoTenhoDia131415) {
             //- bloqueia o pacote pré-conferência+conferência
            $(".tres_dias").addClass("disabled");
            $(".pacote[pacote=completo]").addClass("disabled");
        };
    };

    var hideLoading = function(){

        /*
        853 - 862 - 890
        854 - 863 - 892
        855 - 864 - 886
        852 - 865 - 888
        856 - 866 - 894

        857 - 867 - 891
        858 - 868 - 893
        859 - 883 - 887
        860 - 884 - 889
        861 - 885 - 895
        */

        if (tabelaPerfilCongressitaDia13[841].reqOk && 
            tabelaPerfilCongressitaDia14[846].reqOk && 
            tabelaPerfilCongressitaDia15[849].reqOk &&
            tabelaPerfilCongressitaDia1415[872].reqOk &&
            tabelaPerfilCongressitaDia131415[869].reqOk &&

            tabelaPerfilEstudanteDia13[842].reqOk && 
            tabelaPerfilEstudanteDia14[847].reqOk &&
            tabelaPerfilEstudanteDia15[850].reqOk &&
            tabelaPerfilEstudanteDia1415[873].reqOk &&
            tabelaPerfilEstudanteDia131415[870].reqOk &&

            tabelaPerfilMembrosDia13[844].reqOk &&             
            tabelaPerfilMembrosDia14[848].reqOk && 
            tabelaPerfilMembrosDia15[851].reqOk &&
            tabelaPerfilMembrosDia1415[875].reqOk &&
            tabelaPerfilMembrosDia131415[872].reqOk){
            allReqFinish = true;
        }

        count++;
        //console.log(count);
        //console.log(allReqFinish);

        if (allReqFinish && clickedPublico) {
            
            VerificaPacotesDoDia();

            // escondo loading
            $("#loading").hide();

            // exibe passo 2
            $("#step2").show();
            //$(".publico[publico=congressitas]").find(".seta").show();
        };
    };
  
    var onClosedModais = function(este){

        clickedPublico = true;

        // zera todos os passos
        $("#step3").hide();
        $("#step4").hide();
        $("#step5").hide();
        $("#panelPagamento").hide();        

        // Apliado loading aqui no passo 2 devido nova validação
        if (allReqFinish) {
            $("#step2").show();
            $(".publico[publico='" + setPublico + "']").find(".seta").show();
        }else
        {
            $("#loading").show();            
            $("#step2").hide();    
        };        

        // scroll
        $('html, body').animate({scrollTop : $("#step2").offset().top}, 500);
    };
    
    var mensagemAlertEAD = function(){
        $.fancybox({
            'type': 'iframe',
            'href': 'modal/alerta.html',
            'width'           : 550,
            'height'          : 150,
            'padding'         : '55px',
            'centerOnScroll'  : true,
            'autoScale'       : true,
            onClosed    :   function() {
               //onClosedModais($(this));
            }
        });
    }

    var validacao = function(){

        var  boolSetEMsg = true;

        var msg = "Você não concluiu a configuração de sua programação. Selecione uma atividade diferente em cada período.";

        for (var i = window.location.pathname.split("/").length - 1; i >= 0; i--) {
            var global = window.location.pathname.split("/")[i];

            if (global == "ingles" && global == "espanhol") {
                switch(global) {
                    case "ingles":                        
                        msg = "You have not finished configuring your program. Choose a different activity for each period.";
                        break;

                    case "espanhol":                        
                        msg = "Usted no ha concluido la configuración de su programa. Seleccione una actividad diferente en cada periodo.";
                        break;
                }
            };
        };
        
        var objRetorno = { 
            estaOk : true,
            element : []
        };

        var error13 = function() {            
            $("#content13").css({"border-color": "red", "border-width":"1px", "border-style":"solid"});            
            objRetorno.estaOk = false;
            objRetorno.element.push(13);
            if (boolSetEMsg) {
                alert(msg);    
            };            
            boolSetEMsg = false;
        };
        var error14 = function() {            
            $("#content14").css({"border-color": "red", "border-width":"1px", "border-style":"solid"});
            objRetorno.estaOk = false;
            objRetorno.element.push(14);
            if (boolSetEMsg) {
                alert(msg);    
            };            
            boolSetEMsg = false;        
        };
        var error15 = function() {            
            $("#content15").css({"border-color": "red", "border-width":"1px", "border-style":"solid"});
            objRetorno.estaOk = false;
            objRetorno.element.push(15);
            if (boolSetEMsg) {
                alert(msg);    
            };            
            boolSetEMsg = false;        
        };
        
        // valida 
        switch(setPacote) {
            case pacote.avulso:

                switch(parseInt(setDia)) {
                    case 13:                        
                        var Pmanha = $("input:checkbox[name=dia13manha]:checked");
                        var PTarde = $("input:checkbox[name=dia13tarde]:checked");

                        if (!Pmanha.is(":checked")) {                            
                            error13();
                        };
                        if (!PTarde.is(":checked")) {
                            error13();
                        };
                        if (Pmanha.attr("paletra") == PTarde.attr("paletra")) {
                            error13();
                        };
                        break;

                    case 14:                        
                        var Pmanha = $("input:checkbox[name=dia14manha]:checked");
                        var PTarde = $("input:checkbox[name=dia14tarde]:checked");

                        if (!$("input:checkbox[name=dia14manha]:checked").is(":checked")) {
                            error14();
                        };
                        if (!$("input:checkbox[name=dia14tarde]:checked").is(":checked")) {
                            error14();
                        };
                        if (Pmanha.attr("paletra") == PTarde.attr("paletra")) {
                            error14();
                        };
                        break;

                    case 15:                        
                        var Pmanha = $("input:checkbox[name=dia15manha]:checked");
                        var PTarde = $("input:checkbox[name=dia15tarde]:checked");

                        if (!$("input:checkbox[name=dia15manha]:checked").is(":checked")) {
                            error15();
                        };
                        if (!$("input:checkbox[name=dia15tarde]:checked").is(":checked")) {
                            error15();
                        };
                        if (Pmanha.attr("paletra") == PTarde.attr("paletra")) {
                            error15();
                        };
                        break;
                }

                break;

            case pacote.conferencia:
                // valido dia 14 e 15
                
                // dia 14
                var Pmanha14 = $("input:checkbox[name=dia14manha]:checked");
                var PTarde14 = $("input:checkbox[name=dia14tarde]:checked");                
                if (!$("input:checkbox[name=dia14manha]:checked").is(":checked")) {
                    error14();
                };
                if (!$("input:checkbox[name=dia14tarde]:checked").is(":checked")) {
                    error14();
                };
                if (Pmanha14.attr("paletra") == PTarde14.attr("paletra")) {
                    error14();
                }; 

                // dia 15
                var Pmanha15 = $("input:checkbox[name=dia15manha]:checked");
                var PTarde15 = $("input:checkbox[name=dia15tarde]:checked");                
                if (!$("input:checkbox[name=dia15manha]:checked").is(":checked")) {
                    error15();
                };
                if (!$("input:checkbox[name=dia15tarde]:checked").is(":checked")) {
                    error15();
                };
                if (Pmanha15.attr("paletra") == PTarde15.attr("paletra")) {
                    error15();
                }; 

                break;

            case pacote.completo:
                // valido dia 13, 14 e 15

                // dia 13
                var Pmanha13 = $("input:checkbox[name=dia13manha]:checked");
                var PTarde13 = $("input:checkbox[name=dia13tarde]:checked");
                if (!$("input:checkbox[name=dia13manha]:checked").is(":checked")) {
                    error13();
                };
                if (!$("input:checkbox[name=dia13tarde]:checked").is(":checked")) {
                    error13();
                };
                if (Pmanha13.attr("paletra") == PTarde13.attr("paletra")) {
                    error13();
                }; 

                // dia 14
                var Pmanha14 = $("input:checkbox[name=dia14manha]:checked");
                var PTarde14 = $("input:checkbox[name=dia14tarde]:checked");
                if (!$("input:checkbox[name=dia14manha]:checked").is(":checked")) {
                    error14();
                };
                if (!$("input:checkbox[name=dia14tarde]:checked").is(":checked")) {
                    error14();
                };
                if (Pmanha14.attr("paletra") == PTarde14.attr("paletra")) {
                    error14();
                }; 

                // dia 15
                var Pmanha15 = $("input:checkbox[name=dia15manha]:checked");
                var PTarde15 = $("input:checkbox[name=dia15tarde]:checked");
                if (!$("input:checkbox[name=dia15manha]:checked").is(":checked")) {
                    error15();
                };
                if (!$("input:checkbox[name=dia15tarde]:checked").is(":checked")) {
                    error15();
                };
                if (Pmanha15.attr("paletra") == PTarde15.attr("paletra")) {
                    error15();
                };

                break;

            case pacote.apoiofinanceiro:
                
                break;
        }

        return objRetorno;
    };

    var montaUrl = function(){

        var url = null;
        var keyPublico = null;

        var visitaGuiada = $("input:checkbox[name=guiada]:checked").attr("EinstId");
        if (!visitaGuiada) {
            visitaGuiada = "";
        }else{
            visitaGuiada = "," + $("input:checkbox[name=guiada]:checked").attr("EinstId");
        }

        var networking  =  $("input:checkbox[id=recepcao]:checked").attr("EinstId");
        if (!networking) {
            networking = "";
        }else{
            networking = "," + $("input:checkbox[id=recepcao]:checked").attr("EinstId");
        }

        var getCheckSelected = function(string){            
            var objRetorno = "";
            var idManha = $("input:checkbox[name='"+ string +"']:checked")

            for (var i = idManha.length - 1; i >= 0; i--) {
                objRetorno += $(idManha[i]).attr("EinstId") + ",";
            };

            var str = objRetorno;
            str = str.substring(0, str.length - 1);

            return str;
        };


        // ação para cada pacote escolhido
        switch(setPacote) {
            case pacote.avulso:                    
                    switch(parseInt(setDia)) {
                        case 13:

                            var keyPublico = tabelaProdutos[setPublico+"+13"].Id;
                            //var idManha = $("input:checkbox[name=dia13manha]:checked").attr("EinstId");
                            //var idTarde = $("input:checkbox[name=dia13tarde]:checked").attr("EinstId");

                            var idManha = getCheckSelected("dia13manha");
                            var idTarde = getCheckSelected("dia13tarde");

                            url = keyPublico+","+idManha+","+idTarde+visitaGuiada+networking;
                            break;

                        case 14:
                            
                            var keyPublico = tabelaProdutos[setPublico+"+14"].Id;
                            //var idManha = $("input:checkbox[name=dia14manha]:checked").attr("EinstId");
                            //var idTarde = $("input:checkbox[name=dia14tarde]:checked").attr("EinstId");

                            var idManha = getCheckSelected("dia14manha");
                            var idTarde = getCheckSelected("dia14tarde");

                            url = keyPublico+","+idManha+","+idTarde+visitaGuiada+networking;
                            break;

                        case 15:
                            
                            var keyPublico = tabelaProdutos[setPublico+"+15"].Id;
                            //var idManha = $("input:checkbox[name=dia15manha]:checked").attr("EinstId");
                            //var idTarde = $("input:checkbox[name=dia15tarde]:checked").attr("EinstId");

                            var idManha = getCheckSelected("dia15manha");
                            var idTarde = getCheckSelected("dia15tarde");

                            url = keyPublico+","+idManha+","+idTarde+visitaGuiada+networking;
                            break;
                    }

                break;

            case pacote.conferencia:
                
                var keyPublico = tabelaProdutos[setPublico+"+1415"].Id;

                //var idManha14 = $("input:checkbox[name=dia14manha]:checked").attr("EinstId");
                //var idTarde14 = $("input:checkbox[name=dia14tarde]:checked").attr("EinstId");

                //var idManha15 = $("input:checkbox[name=dia15manha]:checked").attr("EinstId");
                //var idTarde15 = $("input:checkbox[name=dia15tarde]:checked").attr("EinstId");


                var idManha14 = getCheckSelected("dia14manha");
                var idTarde14 = getCheckSelected("dia14tarde");

                var idManha15 = getCheckSelected("dia15manha");
                var idTarde15 = getCheckSelected("dia15tarde");

                url = keyPublico+","+idManha14+","+idTarde14+","+idManha15+","+idTarde15+visitaGuiada+networking;

                break;

            case pacote.completo:
                
                var keyPublico = tabelaProdutos[setPublico+"+131415"].Id;

                //var idManha13 = $("input:checkbox[name=dia13manha]:checked").attr("EinstId");
                //var idTarde13 = $("input:checkbox[name=dia13tarde]:checked").attr("EinstId");

                //var idManha14 = $("input:checkbox[name=dia14manha]:checked").attr("EinstId");
                //var idTarde14 = $("input:checkbox[name=dia14tarde]:checked").attr("EinstId");

                //var idManha15 = $("input:checkbox[name=dia15manha]:checked").attr("EinstId");
                //var idTarde15 = $("input:checkbox[name=dia15tarde]:checked").attr("EinstId");

                var idManha13 = getCheckSelected("dia13manha");
                var idTarde13 = getCheckSelected("dia13tarde");

                var idManha14 = getCheckSelected("dia14manha");
                var idTarde14 = getCheckSelected("dia14tarde");

                var idManha15 = getCheckSelected("dia15manha");
                var idTarde15 = getCheckSelected("dia15tarde");

                url = keyPublico+","+idManha13+","+idTarde13+","+idManha14+","+idTarde14+","+idManha15+","+idTarde15+visitaGuiada+networking;

                break;

            case pacote.apoiofinanceiro:				
				if(typeof apoioSet !== 'undefined' && apoioSet == true){
					switch(categoriaSet){
						case "avulso":
							switch(setDia){
								case 13:
									var keyPublico = "1098",
									idManha = getCheckSelected("dia13manha"),
	    	                        idTarde = getCheckSelected("dia13tarde");
								 	url = keyPublico+","+idManha+","+idTarde+networking;
								break;
								
								case 14:
									var keyPublico = "1099",
									idManha = getCheckSelected("dia14manha"),
									idTarde = getCheckSelected("dia14tarde");
									url = keyPublico+","+idManha+","+idTarde+networking;							
								break;
								
								case 15:
									var keyPublico = "1100",
									idManha = getCheckSelected("dia15manha"),
									idTarde = getCheckSelected("dia15tarde");
									url = keyPublico+","+idManha+","+idTarde+networking;
								break;
							}						
						break;
						
						case "conferencia":
							var keyPublico = "1101",
							idManha14 = getCheckSelected("dia14manha"),
							idTarde14 = getCheckSelected("dia14tarde"),
							idManha15 = getCheckSelected("dia15manha"),
							idTarde15 = getCheckSelected("dia15tarde");			
							url = keyPublico+","+idManha14+","+idTarde14+","+idManha15+","+idTarde15+networking;						
						break;
						
						case "completo":
							var keyPublico = "1102",
							idManha13 = getCheckSelected("dia13manha"),
							idTarde13 = getCheckSelected("dia13tarde"),	
							idManha14 = getCheckSelected("dia14manha"),
							idTarde14 = getCheckSelected("dia14tarde"),			
							idManha15 = getCheckSelected("dia15manha"),
							idTarde15 = getCheckSelected("dia15tarde");			
							url = keyPublico+","+idManha13+","+idTarde13+","+idManha14+","+idTarde14+","+idManha15+","+idTarde15+networking;						
						break;
					}
				}
                break;
        }

        return url;
    };

    var resetValidacao = function(){
        $("#content13").css({"border-color": "#d1e0e9", "border-width":"1px", "border-style":"solid"});
        $("#content14").css({"border-color": "#d1e0e9", "border-width":"1px", "border-style":"solid"});
        $("#content15").css({"border-color": "#d1e0e9", "border-width":"1px", "border-style":"solid"});
    };

    var validacaoDeDisponibilidade = function(){

        var temOProduto = function(produto){            
            $.getJSON("http://esitef.einstein.br/pagtoLocal/SPServices.asmx/StatusInscricao?pid="+produto+"&callback=?",function(data){

                switch(data.status){
                    case "OK":
                        tabelaPerfilCongressitaDia13[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilCongressitaDia14[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilCongressitaDia15[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilCongressitaDia1415[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilCongressitaDia131415[data.id] = { temProduto : false, reqOk : true };

                        tabelaPerfilEstudanteDia13[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilEstudanteDia14[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilEstudanteDia15[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilEstudanteDia1415[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilEstudanteDia131415[data.id] = { temProduto : false, reqOk : true };

                        tabelaPerfilMembrosDia13[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilMembrosDia14[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilMembrosDia15[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilMembrosDia1415[data.id] = { temProduto : false, reqOk : true };
                        tabelaPerfilMembrosDia131415[data.id] = { temProduto : false, reqOk : true };

                        tabelaProdutosDia13[data.id] = { temProduto : false, reqOk : true };
                        tabelaProdutosDia14[data.id] = { temProduto : false, reqOk : true };
                        tabelaProdutosDia15[data.id] = { temProduto : false, reqOk : true };
                    break;
                    case "ENC":
                        tabelaPerfilCongressitaDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaPerfilEstudanteDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaPerfilMembrosDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaProdutosDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaProdutosDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaProdutosDia15[data.id] = { temProduto : true, reqOk : true };
                    break;
                    case "ESG":
                        tabelaPerfilCongressitaDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilCongressitaDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaPerfilEstudanteDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilEstudanteDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaPerfilMembrosDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia15[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia1415[data.id] = { temProduto : true, reqOk : true };
                        tabelaPerfilMembrosDia131415[data.id] = { temProduto : true, reqOk : true };

                        tabelaProdutosDia13[data.id] = { temProduto : true, reqOk : true };
                        tabelaProdutosDia14[data.id] = { temProduto : true, reqOk : true };
                        tabelaProdutosDia15[data.id] = { temProduto : true, reqOk : true };
                    break;
                }

                hideLoading();
                
            });
        }

        // Perfil dia 13
            temOProduto(841);            
            temOProduto(842);            
            temOProduto(844);

        // Perfil dia 14
            temOProduto(846);            
            temOProduto(847);            
            temOProduto(848);

        // Perfil dia 15
            temOProduto(849);            
            temOProduto(850);            
            temOProduto(851);

        // Perfil dia 14 e 15
            temOProduto(872);            
            temOProduto(873);            
            temOProduto(875);

        // Perfil dia 13, 14 e 15
            temOProduto(869);            
            temOProduto(870);            
            temOProduto(871);
        
    };

    var controleInscricao = function(){

        var temOProduto = function(produto){
            $.getJSON("http://esitef.einstein.br/pagtoLocal/SPServices.asmx/StatusInscricao?pid="+produto+"&callback=?",function(data){

                count1++;                    
                //console.log(count1);

                /**/
                switch(data.status){
                    case "OK":                            
                        
                    break;
                    case "ENC":
                        $("input:checkbox[einstid='"+ produto +"']").prop('disabled', true);
                        $("input:checkbox[einstid='"+ produto +"']").prop("checked", false);
                        $("li[EinstId='"+ produto +"']").addClass("esgotado");
                    break;
                    case "ESG":
                        $("input:checkbox[einstid='"+ produto +"']").prop('disabled', true);
                        $("input:checkbox[einstid='"+ produto +"']").prop("checked", false);                            
                        $("li[EinstId='"+ produto +"']").addClass("esgotado");
                    break;
                }
                
            });
        }

        var controleDia13 = function(){
            //Manhã                  
            temOProduto(853);
            temOProduto(854);
            temOProduto(855);
            temOProduto(852);
            temOProduto(856);

            //Tarde                  
            temOProduto(857);
            temOProduto(858);
            temOProduto(859);
            temOProduto(860);
            temOProduto(861);
        };

        var controleDia14 = function(){
            //Manhã                  
            temOProduto(862);
            temOProduto(863);
            temOProduto(864);
            temOProduto(865);
            temOProduto(866);

            //Tarde                  
            temOProduto(867);
            temOProduto(868);
            temOProduto(883);
            temOProduto(884);
            temOProduto(885);
        };

        var controleDia15 = function(){
            //Manhã                  
            temOProduto(890);
            temOProduto(892);
            temOProduto(886);
            temOProduto(888);
            temOProduto(894);

            //Tarde                  
            temOProduto(891);
            temOProduto(893);
            temOProduto(887);
            temOProduto(889);
            temOProduto(895);
        };

        var controlVisitaGuiada = function(){
            //Dia inteiro                  
            temOProduto(877);
            temOProduto(878);
            temOProduto(879);
        };

        controleDia13();
        controleDia14();
        controleDia15();
        controlVisitaGuiada();
    };

    // Abas
    $("#tabs > ul.abas > li > a").click(function(evt) {
        evt.preventDefault();
        var clicked = $(this);
        clicked.parent().parent().find("li").removeClass("selected");
        clicked.parent().addClass("selected");
        $("#tabs > .content_aba").hide();
        $(clicked.attr('href')).show();
    });

    // fancy Box Passo 1
    $(".apoio a").fancybox({
        'width'           : 550,
        'height'          : 330,
        'padding'         : '55px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
        'onComplete'  :   function() {
            // remove "selected" de todos
            $(".publico").removeClass("selected");

            // adiciona "selected" apenas do selecionado
            $(".apoio").addClass("selected");
            },
        onClosed    :   function() {
			onClosedModais($(this));
            $(".opcao4").removeClass("selected");
			$(".mensagem_step").addClass("hide");
			$("#mensagemApoio").removeClass("hide");
			escolheuApoio = true;			
			/**/
        }
    });

    // Passo 1 click no cortesia
    $(".cortesia a").fancybox({
        'width'           : 550,
        'height'          : 100,
        'padding'         : '55px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
        onComplete  :   function() {
            // remove "selected" de todos
            $(".publico").removeClass("selected");			
			
            // adiciona "selected" apenas do selecionado
            $(".cortesia").addClass("selected");
        },
        onClosed    :   function() {
            onClosedModais($(this));
            $(".opcao4").addClass("hide");
			$(".mensagem_step").removeClass("hide");
			$("#mensagemApoio").addClass("hide");
			escolheuApoio = false;
        }
    });

    // Passo 1 click no membro
    $(".membro a").fancybox({
        'width'           : 550,
        'height'          : 200,
        'padding'         : '55px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
        onComplete  :   function() {
            // remove "selected" de todos
            $(".publico").removeClass("selected");

            // adiciona "selected" apenas do selecionado
            $(".membro").addClass("selected");

        },      
        onClosed    :   function() {
           onClosedModais($(this));
           $(".opcao4").addClass("hide");
		   $(".mensagem_step").removeClass("hide");
		   $("#mensagemApoio").addClass("hide");
		   escolheuApoio = false;
        }
    });

    // Passo 1 click no eff
    $(".eff a").fancybox({
        'width'           : 550,
        'height'          : 200,
        'padding'         : '55px',
        'centerOnScroll'  : true,
        'autoScale'       : true,
        onComplete  :   function() {
            // remove "selected" de todos
            $(".publico").removeClass("selected");

            // adiciona "selected" apenas do selecionado
            $(".eff").addClass("selected");
        },
        onClosed    :   function() {
           onClosedModais($(this));
           $(".opcao4").removeClass("hide");
		   $(".mensagem_step").removeClass("hide");
			$("#mensagemApoio").addClass("hide");
			escolheuApoio = false;
        }
    });

     // Passo 1 click no congessita
    $(".congressista a").click(function(){
        /*$(".filtro1 li").removeClass("selected");
        $(this).parent().addClass("selected");*/        
        $(".opcao4").addClass("hide");
    });

    // checbox
    $("input:checkbox").on('click', function() {
        var $box = $(this);             
        if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });    

    //1 Passo publico
    $(".publico").click(function() {
        
        $(".publico").find(".seta").hide();

        setPublico = $(this).attr("publico");

        if ($(this).hasClass("congressista")) {
            
            clickedPublico = true;

             // remove "selected" de todos
            $(".publico").removeClass("selected");

            // adiciona "selected" apenas do selecionado
            $(".congressista").addClass("selected");

            onClosedModais();
        };

        return false;
    })

    // 2 Passo publico
    $(".pacote").click(function() {

        if ($(this).hasClass("disabled")) {
            alert("Pacote esgotado.");
            return false;
        };

        /*
        // remove controle de inscritos
        var obj = $(".palestras");
        $.each( obj, function( key, value ) {
            $(value).removeClass("esgotado");
            var obj1 = $(value).find("input");
            $.each( obj1, function( key1, value1 ) {
                $(value1).prop('disabled', false);
            });
        });
        */
    
        // guarda valor selecionado
        setPacote = $(this).attr("pacote");
		
		if(escolheuApoio){
			var urlApoio = "http://ensino.einstein.br/portal/cad-insc-taleo.aspx?pId="
			switch($(this).attr("pacote")){
				case pacote.avulso:
					switch($(this).attr("id")){
						case "dia13":
						window.location.assign(urlApoio + "1098")
						break;
						
						case "dia14":
						window.location.assign(urlApoio + "1099");
						break;
						
						case "dia15":
						window.location.assign(urlApoio + "1100");
						break;			
					}
				break;
							
				case pacote.conferencia:
				window.location.assign(urlApoio + "1101");
				break;
				
				case pacote.completo:
				window.location.assign(urlApoio + "1102");
				break;
			}		
		}else{
			// remove "selected" de todos
			$(".pacote").removeClass("selected");
	
			// adiciona "selected" apenas do selecionado
			$(this).addClass("selected");
			
			// exibe passo 3
			$("#step3").show();
			$("#step4").show();
			$("#step5").show();
			$("#panelPagamento").show();
	
			$('html, body').animate({scrollTop : $("#step3").offset().top}, 500);
	
			// deixa todos os dias none
			$(".dia").hide();
	
			// ação para cada pacote escolhido       
			switch($(this).attr("pacote")) {
				case pacote.avulso:
					setDia = $(this).attr("dia");
					$(".dia[dia='" + $(this).attr("dia") + "']").show();
					break;
	
				case pacote.conferencia:
					$(".dia[dia=14]").show();
					$(".dia[dia=15]").show();
					break;
	
				case pacote.completo:
					$(".dia[dia=13]").show();
					$(".dia[dia=14]").show();
					$(".dia[dia=15]").show();
					break;
	
				case pacote.apoiofinanceiro:
					break;
			}		
		}

        return false;
    })

    // 3 Ação de pagamento
    $("#pagamento").click(function(){

        resetValidacao();

        var objRetorno = validacao();
        if (!objRetorno.estaOk) {
            $('html, body').animate({scrollTop : $("div[dia='"+ objRetorno.element[0] +"']").offset().top}, 1000);
            return false;
        };

        var param = montaUrl();

        // hide panel
        $("#step2").hide();
        $("#step3").hide();
        $("#step4").hide();
        $("#step5").hide();
        $("#panelPagamento").hide();
		if(typeof apoioSet !== 'undefined' && apoioSet == true){
			$(".seta").attr("style","display:none;");
			window.location.assign("index.html");
		}	

        // unchecked
        var group = "input:checkbox[name=dia13manha]";
        $(group).prop("checked", false);
        group = "input:checkbox[name=dia13tarde]";
        $(group).prop("checked", false);
        
        group = "input:checkbox[name=dia14manha]";
        $(group).prop("checked", false);
        group = "input:checkbox[name=dia14tarde]";
        $(group).prop("checked", false);

        group = "input:checkbox[name=dia15manha]";
        $(group).prop("checked", false);
        group = "input:checkbox[name=dia15tarde]";
        $(group).prop("checked", false);

        // unselected
        $(".publico").removeClass("selected");
        $(".pacote").removeClass("selected");

        
        var region = "ptb"
        for (var i = window.location.pathname.split("/").length - 1; i >= 0; i--) {
            var global = window.location.pathname.split("/")[i];

            if (global == "ingles" && global == "espanhol") {
                switch(global) {
                    case "ingles":                        
                        region = "enu";
                        break;

                    case "espanhol":                        
                        region = "enu";
                        break;
                }
            };
        };

        
        window.open("https://einsteinbr.learn.taleo.net/custom/customloginpage.asp?ecommerce=true&language="+ region +"&page=2106&secure=true&pid=" + param);

        return false;
    });

    // inicia validação inputs dia 13
    $("input:checkbox[EinstId=853]").change(function(){  if ($("input:checkbox[EinstId=853]").is(":checked")) {  if ($("input:checkbox[EinstId=857]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=857]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=857]").change(function(){  if ($("input:checkbox[EinstId=857]").is(":checked")) {  if ($("input:checkbox[EinstId=853]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=853]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=854]").change(function(){  if ($("input:checkbox[EinstId=854]").is(":checked")) {  if ($("input:checkbox[EinstId=858]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=858]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=858]").change(function(){  if ($("input:checkbox[EinstId=858]").is(":checked")) {  if ($("input:checkbox[EinstId=854]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=854]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=855]").change(function(){  if ($("input:checkbox[EinstId=855]").is(":checked")) {  if ($("input:checkbox[EinstId=859]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=859]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=859]").change(function(){  if ($("input:checkbox[EinstId=859]").is(":checked")) {  if ($("input:checkbox[EinstId=855]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=855]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=852]").change(function(){  if ($("input:checkbox[EinstId=852]").is(":checked")) {  if ($("input:checkbox[EinstId=860]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=860]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=860]").change(function(){  if ($("input:checkbox[EinstId=860]").is(":checked")) {  if ($("input:checkbox[EinstId=852]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=852]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=856]").change(function(){  if ($("input:checkbox[EinstId=856]").is(":checked")) {  if ($("input:checkbox[EinstId=861]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=861]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=861]").change(function(){  if ($("input:checkbox[EinstId=861]").is(":checked")) {  if ($("input:checkbox[EinstId=856]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=856]").prop("checked", false);  };  });

    //dia 14
    $("input:checkbox[EinstId=862]").change(function(){  if ($("input:checkbox[EinstId=862]").is(":checked")) {  if ($("input:checkbox[EinstId=867]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=867]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=867]").change(function(){  if ($("input:checkbox[EinstId=867]").is(":checked")) {  if ($("input:checkbox[EinstId=862]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=862]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=863]").change(function(){  if ($("input:checkbox[EinstId=863]").is(":checked")) {  if ($("input:checkbox[EinstId=868]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=868]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=868]").change(function(){  if ($("input:checkbox[EinstId=868]").is(":checked")) {  if ($("input:checkbox[EinstId=863]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=863]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=864]").change(function(){  if ($("input:checkbox[EinstId=864]").is(":checked")) {  if ($("input:checkbox[EinstId=883]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=883]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=883]").change(function(){  if ($("input:checkbox[EinstId=883]").is(":checked")) {  if ($("input:checkbox[EinstId=864]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=864]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=865]").change(function(){  if ($("input:checkbox[EinstId=865]").is(":checked")) {  if ($("input:checkbox[EinstId=884]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=884]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=884]").change(function(){  if ($("input:checkbox[EinstId=884]").is(":checked")) {  if ($("input:checkbox[EinstId=865]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=865]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=866]").change(function(){  if ($("input:checkbox[EinstId=866]").is(":checked")) {  if ($("input:checkbox[EinstId=885]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=885]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=885]").change(function(){  if ($("input:checkbox[EinstId=885]").is(":checked")) {  if ($("input:checkbox[EinstId=866]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=866]").prop("checked", false);  };  }); 
    
    //dia 15
    $("input:checkbox[EinstId=890]").change(function(){  if ($("input:checkbox[EinstId=890]").is(":checked")) {  if ($("input:checkbox[EinstId=891]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=891]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=891]").change(function(){  if ($("input:checkbox[EinstId=891]").is(":checked")) {  if ($("input:checkbox[EinstId=890]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=890]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=892]").change(function(){  if ($("input:checkbox[EinstId=892]").is(":checked")) {  if ($("input:checkbox[EinstId=893]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=893]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=893]").change(function(){  if ($("input:checkbox[EinstId=893]").is(":checked")) {  if ($("input:checkbox[EinstId=892]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=892]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=886]").change(function(){  if ($("input:checkbox[EinstId=886]").is(":checked")) {  if ($("input:checkbox[EinstId=887]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=887]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=887]").change(function(){  if ($("input:checkbox[EinstId=887]").is(":checked")) {  if ($("input:checkbox[EinstId=886]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=886]").prop("checked", false);  };  }); 
    $("input:checkbox[EinstId=888]").change(function(){  if ($("input:checkbox[EinstId=888]").is(":checked")) {  if ($("input:checkbox[EinstId=889]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=889]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=889]").change(function(){  if ($("input:checkbox[EinstId=889]").is(":checked")) {  if ($("input:checkbox[EinstId=888]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=888]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=894]").change(function(){  if ($("input:checkbox[EinstId=894]").is(":checked")) {  if ($("input:checkbox[EinstId=895]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=895]").prop("checked", false);  };  });
    $("input:checkbox[EinstId=895]").change(function(){  if ($("input:checkbox[EinstId=895]").is(":checked")) {  if ($("input:checkbox[EinstId=894]").is(":checked")) { mensagemAlertEAD(); }; $("input:checkbox[EinstId=894]").prop("checked", false);  };  });


    validacaoDeDisponibilidade();

    // controle de inscritos
    controleInscricao();

});