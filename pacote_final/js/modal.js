// JavaScript Document
$(function(){
	$('.cpf').mask('000.000.000-00', {reverse: true});
			
        $('form#qued-form').click(function(event){
            event.preventDefault();
            $.post('http://apps.einstein.br/qued/qued-wb-submit2.asp', $(this).serialize(), function(data){
                if(data.ok){
                    $('p.msg').text();
                    $('p.msg').removeClass('erro').addClass('sucesso').text('Seu formulário foi enviado com sucesso.');
                    $('div.qued-form').html('');
                    $('div.qued-form').html('<p class="msg sucesso">Seu formulário foi enviado com sucesso.</p><p class="avise-me"><a class="qued-bt" href="" target="_parent">Fechar</p>');
                }else{
                    $('p.msg').text();
                    $('p.msg').removeClass('sucesso').addClass('erro').text('Por favor, responda as perguntas obrigatórias.');																								  				
                }
            });
        });
        /**/
        $('#profissoes').change(function(){
            if($(this).val() == 'outros'){
                $('input#3236').val('');
                $('div.profissao').fadeIn();
            }else{
                $('div.profissao').fadeOut();
                $('input#3236').val($(this).val());
            }
            if($(this).val() == 'médico'){
                $('#c3237').fadeIn();
            }else{
                $('#c3237').fadeOut();
            }
        })
        /**/
        $('#esp-group input').click(atualizaEspecialidades);
        function atualizaEspecialidades() {   
            var allVals = [];
            $('#esp-group :checked').each(function() {
                allVals.push($(this).val());
            });
            $('#c3237 input').val(allVals);
        }
    });