// JavaScript Document
$.fn.novidades = function() {
$(this).click(function(){
	var nome = $("#c4818 input").val(),
	email = $("#c1156 input").val(),
	cargo = $("#c247 input").val(),
	empresa = $("#c3405 input").val(),
	filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
	idioma = $("#qued-form").attr("class");
	console.log(nome);
	
	if(nome == "Nome completo" || nome.length < 2 || nome == "Name" || nome == "Nombre"){
		$(".aviso").fadeOut("slow");
		switch(idioma){
			case "pt":				
				$(this).parent().parent().find("#c4818 .aviso").text("Por favor, preencha seu nome").fadeIn("slow");			
			break;
			
			case "en":
				$(this).parent().parent().find("#c4818 .aviso").text("Please, fill in your name").fadeIn("slow");	
			break;
			
			case "es":
				$(this).parent().parent().find("#c4818 .aviso").text("Por favor, escriba su nombre").fadeIn("slow");	
			break;
		}

	}else if(!filter.test(email)){
		$(".aviso").fadeOut("slow");
		switch(idioma){
			case "pt":
				$(this).parent().parent().find("#c1156 .aviso").text("Por favor, preencha seu e-mail").fadeIn("slow");
			break;
			
			case "en":
				$(this).parent().parent().find("#c1156 .aviso").text("Please, fill in your email").fadeIn("slow");
			break;
			
			case "es":
				$(this).parent().parent().find("#c1156 .aviso").text("Por favor, introduzca su e-mail").fadeIn("slow");
			break;
		}		
	}else if(cargo == "Cargo" || cargo == "Job Title" || cargo == "Título Profesional"){
		$(".aviso").fadeOut("slow");
		switch(idioma){
			case "pt":
				$(this).parent().parent().find("#c247 .aviso").text("Por favor, preencha seu cargo").fadeIn("slow");
			break;
			
			case "en":
				$(this).parent().parent().find("#c247 .aviso").text("Please, fill in your job title").fadeIn("slow");
			break;
			
			case "es":
				$(this).parent().parent().find("#c247 .aviso").text("Por favor, escriba su título profesional").fadeIn("slow");
			break;
		}		
	}else if(empresa == "Empresa" || empresa == "Company" || empresa == "Negocios"){
		$(".aviso").fadeOut("slow");
		switch(idioma){
			case "pt":
				$(this).parent().parent().find("#c3405 .aviso").text("Por favor, preencha o nome de sua empresa").fadeIn("slow");
			break;
			
			case "en":
				$(this).parent().parent().find("#c3405 .aviso").text("Please, fill in the name of your company").fadeIn("slow");
			break;
			
			case "es":
				$(this).parent().parent().find("#c3405 .aviso").text("Por favor, escriba el nombre de su empresa").fadeIn("slow");
			break;
		}		
		
	}else{
		$.ajax({
			type:"POST",
			url:"http://apps.einstein.br/qued/qued-wb-submit2.asp",
			data:$("#qued-form input").serialize(),
			success:function(data){
				$("#qued-form").hide();
				switch(idioma){
					case "pt":
						$(".novidades .aviso.verde").text("Obrigado por se cadastrar").fadeIn("slow");
					break;
					
					case "en":
						$(".novidades .aviso.verde").text("Thank you for registering").fadeIn("slow");
					break;
					
					case "es":
						$(".novidades .aviso.verde").text("Gracias por registrarse").fadeIn("slow");
					break;	
				}				
			}
		});
	}
});
}	  

$("#bt264").novidades();
$("#bt265").novidades();
