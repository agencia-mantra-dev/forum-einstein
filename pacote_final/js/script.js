$.fn.novidades = function() {
$(this).click(function(){
	var container = $(this).parent().parent().parent().attr("id"),
	nome = $("#" + container + " #qued-form #c4818 input").val(),
	email = $("#" + container + " #qued-form #c1156 input").val(),
	cargo = $("#" + container + " #qued-form #c247 input").val(),
	empresa = $("#" + container + " #qued-form #c3405 input").val(),
	filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	
	if(nome == "" || nome == "Nome completo"){
		$(".aviso").fadeOut("slow");
		 $("#" + container + " #qued-form #c4818 .aviso").text("Por favor, preencha seu nome").fadeIn("slow");
	}else if(!filter.test(email)){
		$(".aviso").fadeOut("slow");
		$("#" + container + " #qued-form #c1156 .aviso").text("Por favor, preencha seu e-mail").fadeIn("slow");
	}else if(cargo == "" || cargo == "Cargo"){
		$(".aviso").fadeOut("slow");
		$("#" + container + " #qued-form #c247 .aviso").text("Por favor, preencha seu cargo").fadeIn("slow");
	}else if(empresa == "" || empresa == "Empresa"){
		$(".aviso").fadeOut("slow");
		$("#" + container + " #qued-form #c3405 .aviso").text("Por favor, preencha o nome de sua empresa").fadeIn("slow");
	}else{
		$.ajax({
			type:"POST",
			url:"http://apps.einstein.br/qued/qued-wb-submit2.asp",
			data:$("#" + container +  " #qued-form input").serialize(),
			success:function(data){
				$("#" + container +  " #qued-form").hide();
				$("#" + container +  " .aviso.verde").text("Obrigado por se cadastrar").fadeIn("slow");
			}
		});
	}	
});
}	  

$("#bt264").novidades();
$("#bt265").novidades();
