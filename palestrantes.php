<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Palestrantes | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/palestrantes.css" />
        <link rel="stylesheet" href="css/tooltipster.css" />
        <link rel="stylesheet" href="css/fancybox.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="ingles/index.php" class="eng">English</a></li>
                        <li><a href="espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> <span>como chegar</span></a></li>
                        <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> <span>hospedagem</span></a></li>
                        <li><a href="contato.php"><i class="fa fa-envelope"></i> <span>contato</span></a></li>
                        <li class="last"><a href="duvidas.php"><i class="fa fa-question-circle"></i> <span>dúvidas frequentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="evento.php">evento</a></li>
                        <li><a href="programacao.php">programação</a></li>
                        <li><a href="palestrantes.php" class="select">palestrantes</a></li>
                        <li><a href="inscricoes.php">inscrições</a></li>
                        <li class="last"><a href="trabalhos.php">trabalhos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>palestrantes</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central palestrantes">
                <div class="col_left">
                    <div class="holder">
                        <div class="item">
                            <a href="modal/donald_berwick.html" class="modal iframe">
                                <img src="images/palestrantes/donald_berwick.jpg" alt="Donald M. Berwick" title="Donald M. Berwick" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Donald M. Berwick</li>
                                <li class="cargo">Presidente Emérito e Senior Fellow</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/claudio_lottenberg.html" class="modal iframe">
                                <img src="images/palestrantes/claudio_lottenberg.jpg" alt="Claudio Lottenberg" title="Claudio Lottenberg" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Claudio Lottenberg</li>
                                <li class="cargo">Presidente</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/dan_heath.html" class="modal iframe">
                                <img src="images/palestrantes/dan_heath.jpg" alt="Dan Heath" title="Dan Heath" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Dan Heath</li>
                                <li class="cargo">Senior Fellow</li>
                                <li class="empresa">Duke University’s CASE Center</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/maureen_bisognano.html" class="modal iframe">
                                <img src="images/palestrantes/maureen_bisognano.jpg" alt="Maureen Bisognano" title="Maureen Bisognano" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Maureen Bisognano</li>
                                <li class="cargo">Presidente e CEO</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/gary_cohen.html" class="modal iframe">
                                <img src="images/palestrantes/gary_cohen.jpg" alt="Gary Cohen" title="Gary Cohen" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Gary Cohen</li>
                                <li class="cargo">Diretor Executivo</li>
                                <li class="empresa">Health Care Without Harm (HCWH)</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/ademir_petenate.html" class="modal iframe">
                                <img src="images/palestrantes/ademir_petenate.jpg" alt="Ademir José Petenate" title="Ademir José Petenate" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Ademir José Petenate</li>
                                <li class="cargo">Prof. Unicamp</li>
                                <li class="empresa">Departamento de Estatística</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/frank_federico.html" class="modal iframe">
                                <img src="images/palestrantes/frank_federico.jpg" alt="Frank Federico" title="Frank Federico" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Frank Federico</li>
                                <li class="cargo">Diretor Executivo e Strategic Partners</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/gary_kaplan.html" class="modal iframe">
                                <img src="images/palestrantes/gary_kaplan.jpg" alt="Gary S. Kaplan" title="Gary S. Kaplan" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Gary S. Kaplan</li>
                                <li class="cargo">Presidente e CEO</li>
                                <li class="empresa">Sistema de Saúde Virginia Mason</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/henrique_neves.html" class="modal iframe">
                                <img src="images/palestrantes/henrique_neves.jpg" alt="Henrique Neves" title="Henrique Neves" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Henrique Neves</li>
                                <li class="cargo">Diretor Geral</li>
                                <li class="empresa">Sociedade Beneficente Israelita Brasileira Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/jeffrey_simmons.html" class="modal iframe">
                                <img src="images/palestrantes/jeffrey_simmons.jpg" alt="Jeffrey Simmons" title="Jeffrey Simmons" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Jeffrey Simmons</li>
                                <li class="cargo">Médico</li>
                                <li class="empresa"> Cincinnati Children’s Hospital Medical Center</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/joseph_crane.html" class="modal iframe">
                                <img src="images/palestrantes/joseph_crane.jpg" alt="Joseph T. Crane" title="Joseph T. Crane" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Joseph T. Crane</li>
                                <li class="cargo">Diretor Médico</li>
                                <li class="empresa">Mid-Atlantic Permanente Medical Group</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/lisa_schilling.html" class="modal iframe">
                                <img src="images/palestrantes/lisa_schiling.jpg" alt="Lisa Schilling" title="Lisa Schilling" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Lisa Schilling</li>
                                <li class="cargo">Vice-Presidente  Nacional</li>
                                <li class="empresa">Kaiser Permanente</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/marcos_tucherman.html" class="modal iframe">
                                <img src="images/palestrantes/marcos_tucherman.jpg" alt="Marcos Tucherman" title="Marcos Tucherman" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Marcos Tucherman</li>
                                <li class="cargo">Gerente de Sustentabilidade</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/miguel_neto.html" class="modal iframe">
                                <img src="images/palestrantes/miguel_neto.jpg" alt="Miguel Cendoroglo Neto" title="Miguel Cendoroglo Neto" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Miguel Cendoroglo Neto</li>
                                <li class="cargo">Diretor Médico</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/paulo_borem.html" class="modal iframe">
                                <img src="images/palestrantes/paulo_borem.jpg" alt="Paulo Borem" title="Paulo Borem" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Paulo Borem</li>
                                <li class="cargo">Coordenador</li>
                                <li class="empresa">Sistema Unimed Brasil</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/pedro_delgado.html" class="modal iframe">
                                <img src="images/palestrantes/pedro_delgado.jpg" alt="Pedro Delgado" title="Pedro Delgado" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Pedro Delgado</li>
                                <li class="cargo">Diretor executivo</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/renilson_rehem.html" class="modal iframe">
                                <img src="images/palestrantes/renilson_rehem.jpg" alt="Renilson Rehem" title="Renilson Rehem" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Renilson Rehem</li>
                                <li class="cargo">Superintendente Executivo</li>
                                <li class="empresa">Hospital da Criança de Brasília</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/shahab_saeed.html" class="modal iframe">
                                <img src="images/palestrantes/shahab_saeed.jpg" alt="Shahab Saeed" title="Shahab Saeed" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Shahab Saeed</li>
                                <li class="cargo">Vice Presidente e COO</li>
                                <li class="empresa">Questar Energy Services</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="modal/somava_stout.html" class="modal iframe">
                                <img src="images/palestrantes/somava_stout.jpg" alt="Somava S. Stout" title="Somava S. Stout" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Somava S. Stout</li>
                                <li class="cargo">Líder Executiva Externa</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                    </div>
                    <p class="obs"><em>Levando em conta a política do IHI, os professores participantes deste congresso deverão informar no início de suas apresentações qualquer interesse econômico ou pessoal que crie ou que possa ser entendido como criador de conflitos relacionados ao material em discussão. O objetivo de tal informação não é o de evitar a apresentação de um palestrante que possua qualquer interesse financeiro significativo, mas sim o de informar aos ouvintes para que estes façam seu próprio julgamento. A não ser quando notificado acima, cada palestrante forneceu todas as informações e não pretende discutir o uso não aprovado/experimental de um produto ou dispositivo comercial e não possui nenhum interesse financeiro significativo para realizar qualquer promoção. Caso usos não aprovados de produtos venham a ser discutidos, os palestrantes deverão informar o fato aos participantes.</em></p>
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip"  title="Marque este evento no seu calendário"></i> <i class="fa fa-calendar"></i></a> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais.</p>
                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery.tooltipster.min.js"></script>
        <script src="js/plugin/jquery.fancybox.js"></script>
        <script src="js/all.js"></script>
        <script src="js/palestrantes.js"></script>

    </body>
</html>