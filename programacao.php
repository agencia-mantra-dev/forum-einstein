<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Programação | 1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta property="og:title" content="1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade" />
        <meta property="og:description" content="Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais" />
        <meta property="og:image" content="http://www.einstein.br/forumqualidadeseguranca/images/share_face.jpg" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/programacao.css" />
        <link rel="stylesheet" href="css/tooltipster.css" />
        <link rel="stylesheet" href="css/fancybox.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <a class="mn_button"><i class="fa fa-bars"></i></a>
                    <ul class="mn_einstein">
                        <li><a href="http://www.einstein.br/hospital/Paginas/hospital.aspx" target="_blank">Hospital</a></li>
                        <li><a href="http://www.einstein.br/exames/Paginas/home.aspx" target="_blank">Exames</a></li>
                        <li><a href="http://www.einstein.br/ensino/Paginas/ensino.aspx" target="_blank">Ensino</a></li>
                        <li><a href="http://www.einstein.br/pesquisa/Paginas/pesquisa.aspx" target="_blank">Pesquisa</a></li>
                        <li><a href="http://www.einstein.br/responsabilidade-social/Paginas/Responsabilidade-social.aspx" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="http://www.einstein.br/einstein-saude/Paginas/einstein-saude.aspx" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="ingles/index.php" class="eng">English</a></li>
                        <li><a href="espanhol/index.php" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="holder_lgo">
                    <img src="images/logos.png" class="logos" alt="" title="" width="257" height="55">
                    <span class="logo"><a href="index.php">1º Fórum Latino Americano de Qualidade e Segurança na Saúde - Em busca da sustentabilidade</a></span>
                </div>
                <div class="holder_menu">
                    <ul class="mn_azul">
                        <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> <span>como chegar</span></a></li>
                        <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> <span>hospedagem</span></a></li>
                        <li><a href="contato.php"><i class="fa fa-envelope"></i> <span>contato</span></a></li>
                        <li class="last"><a href="duvidas.php"><i class="fa fa-question-circle"></i> <span>dúvidas frequentes</span></a></li>
                    </ul>
                    <hr>
                    <ul class="mn_princ">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="evento.php">evento</a></li>
                        <li><a href="programacao.php" class="select">programação</a></li>
                        <li><a href="palestrantes.php">palestrantes</a></li>
                        <li><a href="inscricoes.php">inscrições</a></li>
                        <li class="last"><a href="trabalhos.php">trabalhos</a></li>
                    </ul>
                </div>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>programação</h1>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central programacao">
                <div class="col_left">
                    
                    <div id="tabs">
                        <ul class="abas">
                            <li class="dia1 selected"><a href="#dia1"><span>DIA 1<br>13 de agosto de 2015</span><span class="dia_mob">13</span></a></li>
                            <li class="dia2"><a href="#dia2"><span>DIA 2<br>14 de agosto de 2015</span><span class="dia_mob">14</span></a></li>
                            <li class="dia3"><a href="#dia3"><span>DIA 3<br>15 de agosto de 2015</span><span class="dia_mob">15</span></a></li>
                            <li class="dia4"><a href="#dia4"><span>DIA 4<br>16 de agosto de 2015</span><span class="dia_mob">16</span></a></li>
                        </ul>
                        <div id="dia1" class="content_aba">
                            <img src="images/seta01.png" class="seta">
                            <p><a href="vcs/forum_dia1.vcs" target="_blank"><i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;Adicione este evento ao seu calendário</a></p>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 8h - 9h</p>
                                <p class="tit">Credenciamento</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h - 13h</p>
                                <p class="tit">Intensivas e Simultâneas</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Mensurando e usando dados para melhorias – Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/pedro_delgado.html" class="modal iframe">Pedro Delgado</a> | <a href="modal/paulo_borem.html" class="modal iframe">Paulo Borem</a> | <a href="modal/ademir_petenate.html" class="modal iframe">Ademir Patenate</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Criando um sistema para segurança – Trilha: Segurança do paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/frank_federico.html" class="modal iframe">Frank Federico</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Impactos ambientais para a sustentabilidade da saúde – Trilha: Sustentabilidade</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/marcos_tucherman.html" class="modal iframe">Marcos Tucherman</a> | Practice Greenhealth ou Healthcare without Harm</span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. A função dos líderes no desenvolvimento de uma cultura de melhoria contínua – Trilha: Liderança</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/shahab_saeed.html" class="modal iframe">Shahab Saeed</a> | Palestrante Latino Americano</span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Melhorando a experiência do atendimento ao paciente enquanto se reduz custos – Trilha: Custos e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/joseph_crane.html" class="modal iframe">Joseph Crane</a> | <a href="modal/miguel_neto.html" class="modal iframe">Miguel Cendoroglo</a> | Palestrante Latino Americano</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 13h - 14h</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 14h - 18h</p>
                                <p class="tit">Intensivas e Simultâneas</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Mensurando e usando dados para melhorias – Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/pedro_delgado.html" class="modal iframe">Pedro Delgado</a> | <a href="modal/paulo_borem.html" class="modal iframe">Paulo Borem</a> | <a href="modal/ademir_petenate.html" class="modal iframe">Ademir Patenate</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Criando um sistema para segurança – Trilha: Segurança do paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/frank_federico.html" class="modal iframe">Frank Federico</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Impactos ambientais para a sustentabilidade da saúde – Trilha: Sustentabilidade</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/marcos_tucherman.html" class="modal iframe">Marcos Tucherman</a> | Practice Greenhealth ou Healthcare without Harm</span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. A função dos líderes no desenvolvimento de uma cultura de melhoria contínua – Trilha: Liderança</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/shahab_saeed.html" class="modal iframe">Shahab Saeed</a> | Palestrante Latino Americano</span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Melhorando a experiência do atendimento ao paciente enquanto se reduz custos – Trilha: Custos e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/joseph_crane.html" class="modal iframe">Joseph Crane</a> | <a href="modal/miguel_neto.html" class="modal iframe">Miguel Cendoroglo</a> | Palestrante Latino Americano</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 18h - 20h</p>
                                <p class="tit">Recepção e networking</p>
                            </div>
                            <p><em>*Programação e palestrantes sujeitos a alterações sem aviso prévio.</em></p>
                        </div>
                        <div id="dia2" class="content_aba" style="display:none">
                            <img src="images/seta02.png" class="seta">
                            <p><a href="vcs/forum_dia2.vcs" target="_blank"><i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;Adicione este evento ao seu calendário</a></p>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 8h - 9h</p>
                                <p class="tit">Credenciamento e café</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h - 9h30</p>
                                <p class="tit">Cerimônia de boas-vindas e sessão de abertura</p>
                                <p class="subtit">Qualidade e segurança na saúde: Em busca da sustentabilidade</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/claudio_lottenberg.html" class="modal iframe">Claudio Lottenberg</a> | <a href="modal/pedro_delgado.html" class="modal iframe">Pedro Delgado</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h30 - 10h30</p>
                                <p class="tit">Keynote 1 – "Flipping Healthcare"</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/maureen_bisognano.html" class="modal iframe">Maureen Bisognano</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 10h30 - 11h</p>
                                <p class="tit">Perguntas e respostas</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/maureen_bisognano.html" class="modal iframe">Maureen Bisognano</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 11h - 11h30</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 11h30 - 12h45</p>
                                <p class="tit">Workshops Simlutâneos</p>
                                <p class="subtit">Sessão A</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Habilidades da liderança para transformações – Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/maureen_bisognano.html" class="modal iframe">Maureen Bisognano</a> | Andres Aguirre</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Organização para alta confiabilidade –  Trilha: Segurança do paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/jeffrey_simmons.html" class="modal iframe">Jeffrey Simmons</a> | Astolfo Franco</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Acelerando as transformações na saúde com inovação e abordagem enxuta –  Trilha: Estudo de caso</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_kaplan.html" class="modal iframe">Gary Kaplan</a> | <a href="modal/henrique_neves.html" class="modal iframe">Henrique Neves</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. O futuro do atendimento primário –  Trilha: Saúde populacional</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/renilson_rehem.html" class="modal iframe">Renilson Rehem</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Diminuindo os custos e aumentando o valor em uma nova era: Pontos iniciais e abordagens para o sucesso –  Trilha: Custo e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/claudio_lottenberg.html" class="modal iframe">Claudio Lottenberg</a> | <a href="modal/lisa_schilling.html" class="modal iframe">Lisa Shilling</a></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 12h45 - 14h45</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 14h45 - 16h</p>
                                <p class="tit">Workshops Simlutâneos</p>
                                <p class="subtit">Sessão B</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Habilidades da liderança para transformações – Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/maureen_bisognano.html" class="modal iframe">Maureen Bisognano</a> | Andres Aguirre</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Organização para alta confiabilidade –  Trilha: Segurança do paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/jeffrey_simmons.html" class="modal iframe">Jeffrey Simmons</a> | Astolfo Franco</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Acelerando as transformações na saúde com inovação e abordagem enxuta –  Trilha: Estudo de caso</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_kaplan.html" class="modal iframe">Gary Kaplan</a> | <a href="modal/henrique_neves.html" class="modal iframe">Henrique Neves</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. O futuro do atendimento primário –  Trilha: Saúde populacional</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/renilson_rehem.html" class="modal iframe">Renilson Rehem</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Diminuindo os custos e aumentando o valor em uma nova era: Pontos iniciais e abordagens para o sucesso –  Trilha: Custo e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/claudio_lottenberg.html" class="modal iframe">Claudio Lottenberg</a> | <a href="modal/lisa_schilling.html" class="modal iframe">Lisa Shilling</a></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 16h - 16h30</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 16h30 - 17h30</p>
                                <p class="tit">Keynote 2 – Triple Aim e sustentabilidade"<br>(Trilha: Sustentabilidade)</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_cohen.html" class="modal iframe">Gary Cohen</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 17h30 - 18h</p>
                                <p class="tit">Perguntas e respostas</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_cohen.html" class="modal iframe">Gary Cohen</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 18h - 20h</p>
                                <p class="tit">Recepção e networking</p>
                            </div>
                            <p><em>*Programação e palestrantes sujeitos a alterações sem aviso prévio.</em></p>
                        </div>
                        <div id="dia3" class="content_aba" style="display:none">
                            <img src="images/seta03.png" class="seta">
                            <p><a href="vcs/forum_dia3.vcs" target="_blank"><i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;Adicione este evento ao seu calendário</a></p>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h - 10h</p>
                                <p class="tit">Keynote 3</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/donald_berwick.html" class="modal iframe">Donald Berwick</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 10h - 10h30</p>
                                <p class="tit">Perguntas e respostas</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/donald_berwick.html" class="modal iframe">Donald Berwick</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 10h30 - 11h</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 11h - 12h15</p>
                                <p class="tit">Workshops Simultâneos</p>
                                <p class="subtit">Sessão C</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Gestão de talentos: Desenvolvendo lideranças médicas e administrativas para conduzir o Programa de Qualidade e Segurança –  Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_kaplan.html" class="modal iframe">Gary Kaplan</a> | Juan Pablo Uribe</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. O fator humano: A importância crítica de uma equipe eficaz e da comunicação na condução de um atendimento seguro –  Trilha: Segurança do Paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/frank_federico.html" class="modal iframe">Frank Federico</a> | Astolfo Franco</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Cincinnati Children’s Hospital: Alcançando a melhoria rápida através da junção de equipes responsáveis pelo atendimento com a implantação de sistema de níveis  –  Trilha: Estudo de caso</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/jeffrey_simmons.html" class="modal iframe">Jeffrey Simmons</a> | Marcelo Pellizzari</span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. Estratégias de Triple Aim para populações de alto risco e alto custo – Trilha: Saúde populacional</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/somava_stout.html" class="modal iframe">Somava Stout</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Reduzindo custos e melhorando a qualidade: Etapa dois – Trilha: Custo e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/lisa_schilling.html" class="modal iframe">Lisa Shilling</a> | Palestrante Latino Americano</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 12h15 - 14h15</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 14h15 - 15h30</p>
                                <p class="tit">Workshops Simultâneos</p>
                                <p class="subtit">Sessão D</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Gestão de talentos: Desenvolvendo lideranças médicas e administrativas para conduzir o Programa de Qualidade e Segurança –  Trilha: Desenvolvimento de Competências</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/gary_kaplan.html" class="modal iframe">Gary Kaplan</a> | Juan Pablo Uribe</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. O fator humano: A importância crítica de uma equipe eficaz e da comunicação na condução de um atendimento seguro –  Trilha: Segurança do Paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/frank_federico.html" class="modal iframe">Frank Federico</a> | Astolfo Franco</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Cincinnati Children’s Hospital: Alcançando a melhoria rápida através da junção de equipes responsáveis pelo atendimento com a implantação de sistema de níveis  –  Trilha: Estudo de caso</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/jeffrey_simmons.html" class="modal iframe">Jeffrey Simmons</a> | Marcelo Pellizzari</span>
                                    </li>
                                    <li>
                                        <span class="subtit">4. Estratégias de Triple Aim para populações de alto risco e alto custo – Trilha: Saúde populacional</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/somava_stout.html" class="modal iframe">Somava Stout</a></span>
                                    </li>
                                    <li>
                                        <span class="subtit">5. Reduzindo custos e melhorando a qualidade: Etapa dois – Trilha: Custo e valor</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/lisa_schilling.html" class="modal iframe">Lisa Shilling</a> | Palestrante Latino Americano</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 15h30 - 16h</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 16h - 17h</p>
                                <p class="tit">Keynote 4 – "Como mudar quando a mudança é difícil"<br>(Trilha: Desenvolvimento de Competências)</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/dan_heath.html" class="modal iframe">Dan Heath</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 17h - 17h30</p>
                                <p class="tit">Perguntas e respostas</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> <a href="modal/dan_heath.html" class="modal iframe">Dan Heath</a></p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 17h30 -17h45</p>
                                <p class="tit">Apresentação das conclusões e encerramento</p>
                            </div>
                            <p><em>*Programação e palestrantes sujeitos a alterações sem aviso prévio.</em></p>
                        </div>
                        <div id="dia4" class="content_aba" style="display:none">
                            <img src="images/seta04.png" class="seta">
                            <p><a href="vcs/forum_dia4.vcs" target="_blank"><i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;Adicione este evento ao seu calendário</a></p>

                            <p>Visitas Guiadas | Hospital Israelita Albert Einstein</p>
                            <p>Conduzidas por profissionais experientes, as Visitas Guiadas do Hospital Israelita Albert Einstein são uma ótima maneira de conhecer e observar de perto alguns elementos que fazem parte da cultura de excelência da instituição. </p>
                            <p>Com duração aproximada de 3 horas, irão oferecer aos participantes a oportunidade de interagir e aprendem por meio de visitas temáticas. Em grupos restritos de participantes, cada visita terá um foco específico que irá guiar os participantes por elementos importantes desta que é considerada pela revista América Economia como uma das instituições de saúde de referências na América Latina. </p>
                            <p>Escolha a opção que mais se adequa às suas necessidades e venha participar de uma visita guiada pelo Einstein.</p>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 8h – 8h30</p>
                                <p class="tit">Transporte (ponto de encontro WTC – Hospital Israelita Albert Einstein)</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 8h30 – 9h</p>
                                <p class="tit">Welcome coffee</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h – 9h15</p>
                                <p class="tit">Boas-vindas</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h15 – 12h</p>
                                <p class="tit">Visita guiada</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Melhoria de Processos</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Fluxo do Paciente</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Segurança do Paciente na prática assistencial</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 12h – 12h30</p>
                                <p class="tit">Transporte (Hospital Israelita Albert Einstein – WTC)</p>
                            </div>

                            <p><em>*Programação e palestrantes sujeitos a alterações sem aviso prévio.</em></p>
                        </div>
                    </div>
                    
                </div>
                <div class="col_right">
                    <div class="top">
                        <a href="vcs/forum_geral.vcs" target="_blank"><i class="fa fa-plus-circle tooltip"  title="Marque este evento no seu calendário"></i> <i class="fa fa-calendar"></i></a> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Participe do maior fórum da América Latina sobre qualidade e segurança na saúde, com apresentação de renomados palestrantes nacionais e internacionais.</p>
                    <a href="inscricao.php" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=263506743843321&amp;sdk=joey&amp;u=http://www.einstein.br/forumqualidadeseguranca&amp;display=popup&amp;ref=plugin" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?text=Participe do maior fórum da América Latina sobre qualidade e segurança na saúde - http://goo.gl/KfI4aX&amp;source=webclient" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="http://www.agenciamantra.com.br" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="http://www.einstein.br/Paginas/home.aspx" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery.tooltipster.min.js"></script>
        <script src="js/plugin/jquery.fancybox.js"></script>
        <script src="js/all.js"></script>
        <script src="js/programacao.js"></script>

    </body>
</html>